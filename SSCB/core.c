
#include "sscd.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;

local LinkedList chunks=LL_INITIALIZER;
int chunksum=0;
int chunksnum=0;
// local LinkedList stream=LL_INITIALIZER;
BOOL processingstream=FALSE;
int streamsize=0;
int maxstreamsize=0;
Byte *streamdata=NULL;
Byte *placeholder=NULL;

u32 lastsync=0;

local void printpacket(Byte *pkt, int len)
{
if(pkt[1]==3) return;
	printf("-------------------------\n");
	int i=0;
	int c=0;
	for(i=0; i<len; i++)
	{
		if(c >= 16)
		{
			printf("\n");
			c=0;
		}
		printf("%02x ",pkt[i]);
		c++;
	}
	printf("\n");
	printf("-------------------------\n");
	printf("\n");
}

local int packethandler(net_loc dest, Byte *pkt, int len)
{
	if(len < 2) return 0;

	switch(pkt[0])
	{
		case CORE_PACKET:
		{
			switch(pkt[1])
			{
				case CORE_RELHEADER:
				{
					CoreRelHeader *crh=(CoreRelHeader*)pkt;
					// printf("rel pkt id=%u: size=%d\n",crh->packetid,len);
					
					Byte *data=pkt+sizeof(CoreRelHeader);
					int datalen=len-sizeof(CoreRelHeader);
					
					CoreRelAck cra;
					int size=sizeof(CoreRelAck);
					memset(&cra,0,size);
					cra.type=CORE_PACKET;
					cra.subtype=CORE_RELACK;
					cra.packetid=crh->packetid;
					
					net->SendPacket(dest,&cra,size);
					net->ReceivePacket(dest,data,datalen);
					
					return TRUE;
				}
				break;
				case CORE_DISCONNECT:
				{
					printf("disconnect packet recieved: size=%d\n",len);
					DO_CBS(CB_DISCONNECT,DisconnectFunc,());
					
					return TRUE;
				}
				break;
				case CORE_CLUSTER:
				{
					// printf("cluster packet recieved: size=%d\n",len);
					
					int sofar=sizeof(CoreClusterHeader);
					
					CoreClusterHeader *cch=(CoreClusterHeader*)pkt;
					Byte *data=(Byte*)pkt+sizeof(CoreClusterHeader);
					Byte size=cch->size;
					
					// printf("\ncluster: size=%d type=%02x %02x\n",size,data[0],data[1]);
// printpacket(data,size);
					net->ReceivePacket(dest,data,size);
						
					data+=size;
					sofar+=size;
						
					while(sofar < len)
					{
						size=*data;
						// printf("\ncluster: size=%d type=%02x %02x\n",size,data[1],data[2]);
// printpacket(data,size);
						net->ReceivePacket(dest,data+1,size);
						data+=size+1;
						sofar+=size+1;
					}
					
					return TRUE;
				}
				break;
				case CORE_CHUNK:
				case CORE_LASTCHUNK:
				{
				
				
typedef struct CoreChunk
{
	u8 type; //0x00
	u8 subtype; //0x08
	u8 data[];
} CoreChunk;
					Byte* pk=amalloc(len);
					memcpy(pk,pkt,len);
					LLAddLast(&chunks,pk);
			
					chunksum+=len;
					chunksnum++;
				
					if(pkt[1] == CORE_LASTCHUNK)
					{
						
						Byte* npk=amalloc(chunksum-chunksnum*2);
						int copied=0;
						
						Byte* pk2=NULL;
						FOR_EACH_LINK(chunks,pk2,l)
						{
						
							CoreChunk *cc=(CoreChunk*)pk2;
							copied+=
						
							LLRemove(&chunks,pk2);
							afree(pk2);
						}
						
						net->ReceivePacket(dest,streamdata,streamsize);
					}
				}
				break;
				case CORE_STREAM:
				{
					// printf("stream packet recieved: size=%d\n",len);
					
					// if(!processingstream)
					// {
						// processingstream=TRUE;
						// LLEmpty(&stream);
					// LLAdd(&stream,buf);
					// }
					
					CoreStreamHeader *csh=(CoreStreamHeader*)pkt;
					int size=len-sizeof(CoreStreamHeader);
					if(!streamdata)
					{
						maxstreamsize=csh->totalsize;
						streamsize=0;
						streamdata=amalloc(maxstreamsize);
						placeholder=streamdata;
					}
					if(streamsize + size <= maxstreamsize)
					{
						memcpy(placeholder,csh->data,size);
						placeholder+=size;
						streamsize+=size;
// printf("new stream size=%d\n",streamsize);
					}
					else printf("too much stream data sent\n");
					
					if(streamsize >= csh->totalsize)
					{
// printf("streamsize=%d  > totalsize=%d\n",streamsize,csh->totalsize);
					
						net->ReceivePacket(dest,streamdata,streamsize);
					
						placeholder=NULL;
						// afree(streamdata); //FIXME: memory leak, calling this here wouldnt work
						streamdata=NULL;
						streamsize=0;
						maxstreamsize=0;
						
						// processingstream=FALSE;
						// LLEmpty(&stream);
					}
					
	
					return TRUE;
				}
				break;
				case CORE_STREAMCANCEL:
				{
					printf("stream cancel packet recieved: size=%d\n",len);
					
					if(placeholder)
					{
						placeholder=NULL;
						afree(streamdata);
						streamdata=NULL;
						
						// processingstream=FALSE;
						// LLEmpty(&stream);
					}
					else printf("stream cancel packet recieved for nonexistant stream\n");
					
					CoreStreamCancelAck csca;
					int size=sizeof(CoreStreamCancelAck);
					csca.type=CORE_PACKET;
					csca.subtype=CORE_STREAMCANCELACK;
					net->SendPacket(dest,&csca,size);
					
					return TRUE;
				}
				break;
				case CORE_SYNCREQUEST:
				{
					printf("sync request packet recieved: size=%d\n",len);
					
					CoreSyncRequest *cs=(CoreSyncRequest*)pkt;
					printf("sync: time=%d sent=%d recieved=%d\n",
					cs->localtime,
					cs->packetssent,
					cs->packetsreceived);
					
					u32 curtime=current_ticks()/10;

					CoreSyncResponse csr;
					int size=sizeof(CoreSyncResponse);
					memset(&csr,0,size);
					csr.type=CORE_PACKET;
					csr.subtype=CORE_SYNCRESPONSE;
					csr.elapsedtime=curtime-lastsync;
					csr.servertime=curtime;
					net->SendPacket(dest,&csr,size);
					
					lastsync=curtime;
					
					return TRUE;
				}
				break;
			}
		}
		break;
	}

	return FALSE;
}

EXPORT int MM_core(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		if(!lm || !net) return MM_FAIL;
		
		net->AddCorePacket(CORE_RELHEADER,packethandler);
		net->AddCorePacket(CORE_DISCONNECT,packethandler);
		net->AddCorePacket(CORE_CLUSTER,packethandler);
		net->AddCorePacket(CORE_CHUNK,packethandler);
		net->AddCorePacket(CORE_LASTCHUNK,packethandler);
		net->AddCorePacket(CORE_STREAM,packethandler);
		net->AddCorePacket(CORE_STREAMCANCEL,packethandler);
		net->AddCorePacket(CORE_SYNCREQUEST,packethandler);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		net->RemoveCorePacket(CORE_RELHEADER,packethandler);
		net->RemoveCorePacket(CORE_DISCONNECT,packethandler);
		net->RemoveCorePacket(CORE_CLUSTER,packethandler);
		net->RemoveCorePacket(CORE_CHUNK,packethandler);
		net->RemoveCorePacket(CORE_LASTCHUNK,packethandler);
		net->RemoveCorePacket(CORE_STREAM,packethandler);
		net->RemoveCorePacket(CORE_STREAMCANCEL,packethandler);
		net->RemoveCorePacket(CORE_SYNCREQUEST,packethandler);
		
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
