
#ifndef __DATAMAN_H
#define __DATAMAN_H

#define I_DATAMAN "dataman-1"

#define CB_DATAFUNC "datafunc"
typedef void (*DataFunc)(S2DRegisterServer *rs);

typedef struct Idataman
{
	INTERFACE_HEAD_DECL;

	void (*SaveData)(S2DRegisterServer *rs);

} Idataman;

#endif
