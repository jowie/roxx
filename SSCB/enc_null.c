
#include "sscd.h"

local Imodman *mm;
local Inet *net;

typedef struct EncryptData
{
	int key;
	char table[520];
} EncryptData;

local EncryptData *ed=NULL;

local int Encrypt(Byte *d, int n)
{
	if((d[0] == 0x00) && (d[1] == 0x01))
	{
		ed->key=0;
	}
	return n;
}

local int Decrypt(Byte *d, int n)
{
	return n;
}

local void ConnectionInit(net_loc dest)
{
	CoreLoginRequest cl;
	cl.type=CORE_PACKET;
	cl.subtype=CORE_LOGINREQUEST;
	cl.key=0;
	cl.protocol=0x01;
	
	net->SendPacket(dest,&cl,sizeof(CoreLoginRequest));
}

local int packethandler(net_loc dest, Byte *pkt, int len)
{
	switch(pkt[0])
	{
		case CORE_PACKET:
		{
			switch(pkt[1])
			{
				case CORE_LOGINRESPONSE:
				{
					CoreLoginResponse *clr=(CoreLoginResponse*)pkt;
					printf("login response key=%i ~key=%i\n",clr->key,~clr->key);
					
					DO_CBS(CB_CONNECTIONREADY,ConnectionReadyFunc,(dest));
					
					return TRUE;
				}
				break;
			}
		}
	}
}

local Iencrypt ienc=
{
	INTERFACE_HEAD_INIT("enc-null","enc-null"),
	Encrypt, Decrypt
};

EXPORT int MM_enc_null(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
	
		mm=mm2;
		net=mm->GetInterface(I_NET);
		if(!net) return MM_FAIL;
		
		EncryptData *ed=amalloc(sizeof(*ed));
		net->RegisterEncryption(&ienc);
		
		net->AddCorePacket(CORE_LOGINRESPONSE,packethandler);
		mm->RegCallback(CB_CONNECTIONINIT,ConnectionInit);
		
		mm->RegInterface(&ienc);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&ienc)) return MM_FAIL;
		
		mm->UnregCallback(CB_CONNECTIONINIT,ConnectionInit);
		net->RemoveCorePacket(CORE_LOGINRESPONSE,packethandler);
		
		afree(ed);
		
		mm->ReleaseInterface(net);
		
		return MM_OK;
	}
	return MM_FAIL;
}
