
//1) malloc socket
//2) return pointer
//3) malloc handler LLs
//4) handle type
//5) if unhandled handle sized
//6) if unhandled handle unsized
//7) report unhandled
//but what happens if handled random len packet starts with 0x01 and a 0x01 type handler is registered?
//no break results in packet being correctly handled AND incorrectly handled

//the entire exe only needs 1 thread to send, and 1 thread per listening port



//#include <sys/types.h>

#include "sscd.h"

typedef struct sockaddr_in sockaddrin;

#ifdef WIN32
#define close(x) closesocket(x)
#endif

local Imodman *mm;
local Ilogman *lm;
local Iencrypt *enc;

pthread_t sthd;
pthread_t rthd;

local LinkedList socketlist=LL_INITIALIZER;
local LinkedList packethandlers=LL_INITIALIZER;
local LinkedList corehandlers=LL_INITIALIZER;

SocketID sid=0;

typedef struct PacketHandler
{
	int dest;
	Byte type;
	PacketFunc func;
} PacketHandler;

typedef struct SocketInfo
{
	net_loc loc;
	BOOL listenonly;
	SocketID sid;
	SOCKET socket;
	BOOL connected;
} SocketInfo;

local char* SocketErrorDesc(const int Error)
{
#ifdef WIN32
	switch(Error)
	{
		case WSAEINTR:				return "Interrupted system call";
		case WSAEBADF:				return "Bad file number";
		case WSAEACCES:				return "Permission denied";
		case WSAEFAULT:				return "Bad address";
		case WSAEINVAL:				return "Invalid argument";
		case WSAEMFILE:				return "Too many open files";
		case WSAEWOULDBLOCK:		return "Operation would block";
		case WSAEINPROGRESS:		return "Operation now in progress";
		case WSAEALREADY:			return "Operation already in progress";
		case WSAENOTSOCK:			return "Socket operation on non-socket";
		case WSAEDESTADDRREQ:		return "Destination address required";
		case WSAEMSGSIZE:			return "Message too long";
		case WSAEPROTOTYPE:			return "Protocol wrong type for socket";
		case WSAENOPROTOOPT:		return "Protocol not available";
		case WSAEPROTONOSUPPORT:	return "Protocol not supported";
		case WSAESOCKTNOSUPPORT:	return "Socket type not supported";
		case WSAEOPNOTSUPP:			return "Operation not supported on socket";
		case WSAEPFNOSUPPORT:		return "Protocol family not supported";
		case WSAEAFNOSUPPORT:		return "Address family not supported by protocol family";
		case WSAEADDRINUSE:			return "Address already in use";
		case WSAEADDRNOTAVAIL:		return "Can't assign requested address";
		case WSAENETDOWN:			return "Network is down";
		case WSAENETUNREACH:		return "Network is unreachable";
		case WSAENETRESET:			return "Network dropped connection on reset";
		case WSAECONNABORTED:		return "Software caused connection abort";
		case WSAECONNRESET:			return "Connection reset by peer";
		case WSAENOBUFS:			return "No buffer space available";
		case WSAEISCONN:			return "Socket is already connected";
		case WSAENOTCONN:			return "Socket is not connected";
		case WSAESHUTDOWN:			return "Can't send after socket shutdown";
		case WSAETOOMANYREFS:		return "Too many references: can't splice";
		case WSAETIMEDOUT:			return "Connection timed out";
		case WSAECONNREFUSED:		return "Connection refused";
		case WSAELOOP:				return "Too many levels of symbolic links";
		case WSAENAMETOOLONG:		return "File name too long";
		case WSAEHOSTDOWN:			return "Host is down";
		case WSAEHOSTUNREACH:		return "No route to host";
		case WSAENOTEMPTY:			return "Directory not empty";
		case WSAEPROCLIM:			return "Too many processes";
		case WSAEUSERS:				return "Too many users";
		case WSAEDQUOT:				return "Disc quota exceeded";
		case WSAESTALE:				return "Stale NTFS file handle";
		case WSAEREMOTE:			return "Too many levels of remote in path";
		case WSASYSNOTREADY:		return "Network sub-system is unusable";
		case WSAVERNOTSUPPORTED:	return "WinSock DLL cannot support this application";
		case WSANOTINITIALISED:		return "WinSock not initialized";
		case WSAHOST_NOT_FOUND:		return "Host not found";
		case WSATRY_AGAIN:			return "Non-authoritative host not found";
		case WSANO_RECOVERY:		return "Non-recoverable error";
		case WSANO_DATA:			return "No Data";
		default:					return "Not a WinSock error";
	}
#endif
	return NULL;
}

local void printpacket(Byte *pkt, int len, int dest, int core, int send)
{

printf("%s%son %d: size=%d\n",(send ? "send " : "recv "),(core ? "core " : ""),dest,len);
	int i=0;
	int c=0;
	for(i=0; i<len; i++)
	{
		if(c >= 16)
		{
			printf("\n");
			c=0;
		}
		printf("%02x ",pkt[i]);
		c++;
	}
	printf("\n");
}

local SocketInfo* getsocketbyid(SocketID id)
{
	SocketInfo* si=NULL;
	FOR_EACH_LINK(socketlist,si,l)
	{
		if(si->sid=id) return si;
	}

	return NULL;
}

local SocketInfo* getsocketbyloc(net_loc loc)
{
	SocketInfo* si=NULL;
	FOR_EACH_LINK(socketlist,si,l)
	{
		if(si->loc=loc) return si;
	}

	return NULL;
}

local SocketID CreateNewSocket(net_loc loc, BOOL listenonly)
{
	sid++;
	
	//create socket
	SocketInfo* si=amalloc(sizeof(SocketInfo));
	si->socket=INVALID_SOCKET;
	si->socket=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if(si->socket == -1) lm->Log(L_WARN,"CreateNewSocket socket() failed");
	
#ifdef WIN32
	u_long noblock=1;
	if(ioctlsocket(si->socket,FIONBIO,&noblock) != 0)
	{
		lm->Log(L_WARN,"bad ioctlsocket: %s",SocketErrorDesc(WSAGetLastError()));
		printf("ioctlsocket failed: errno='%s'\n",strerror(errno));
	}
#else
	if(set_nonblock(si->socket) < 0) lm->Log(L_WARN,"<net> can't make socket nonblocking");
#endif
	
	// strncpy(si->name,name,20);
	si->loc=loc;
	si->sid=sid;
	si->listenonly=listenonly;
	
	LLAddLast(&socketlist,si);
	
	return sid;
}

local BOOL ConnectToSocket(SocketID id, char* ip, int port)
{
	SocketInfo* si=getsocketbyid(id);
	if(!si)
	{
		lm->Log(L_ERROR,"<net> cant connect nonexistent socket %i to port %i",id,port);
		return FALSE;
	}

	sockaddrin sain;
	sain.sin_family=AF_INET;
	if(si->listenonly) sain.sin_addr.s_addr=INADDR_ANY;
	else sain.sin_addr.s_addr=inet_addr(ip);
	sain.sin_port=htons(port);
	si->connected=FALSE;
	
	if(si->listenonly)
	{
		if(bind(si->socket,(SOCKADDR*)&sain,sizeof(sain)) == -1)
		{
			lm->Log(L_ERROR,"<net> can't bind socket port %i",port);

			// closesocket(si->socket); //causes conflicting types compile errors on devcpp
		}
		else si->connected=TRUE;
	}
	else
	{
		if(connect(si->socket,(SOCKADDR*)&sain,sizeof(sain)) == -1) lm->Log(L_WARN,"ConnectToSocket connect() failed: %i",si->loc);
		else si->connected=TRUE;
	}
	
	return si->connected;
}

local void AddPacket(net_loc loc, Byte type, PacketFunc func)
{
	PacketHandler *ph=amalloc(sizeof(PacketHandler));
	ph->type=type;
	ph->func=func;
	LLAddLast(&packethandlers,ph);
}

local void RemovePacket(net_loc loc, Byte type, PacketFunc func)
{
	PacketHandler *ph;
	FOR_EACH_LINK(packethandlers,ph,l)
	{
		if((ph->type == type) && (ph->func == func))
		{
			LLRemove(&packethandlers,ph);
			break;
		}
	}
}

local void AddCorePacket(Byte type, PacketFunc func)
{
	PacketHandler *ph=amalloc(sizeof(PacketHandler));
	ph->type=type;
	ph->func=func;
	LLAddLast(&corehandlers,ph);
}

local void RemoveCorePacket(Byte type, PacketFunc func)
{
	PacketHandler *ph;
	FOR_EACH_LINK(corehandlers,ph,l)
	{
		if((ph->type == type) && (ph->func == func))
		{
			LLRemove(&corehandlers,ph);
			break;
		}
	}
}

local BOOL SendPacket(net_loc loc, void *vp, int size)
{
	SocketInfo* si=getsocketbyloc(loc);
	if(!si)
	{
		lm->Log(L_ERROR,"<net> cant send to nonexistent socket at loc %i",loc);
		return FALSE;
	}

	Byte *pkt=vp;

	if(enc) enc->Encrypt(pkt,size);
	
	if(send(si->socket,(char*)pkt,size,0) == -1)
	{
		printf("send failed: %s\n",SocketErrorDesc(WSAGetLastError()));
		printf("send failed: errno='%s'\n",strerror(errno));
	}
	
	// afree(pkt);
	return TRUE;
}

local void ReceivePacket(net_loc dest, void *vp, int len)
{
	if(len < 1) return;
	
	Byte *pkt=vp;
	Byte type=pkt[0];
	Byte subtype=0;
	if(len >= 2) subtype=pkt[1];
	
	if(type == CORE_PACKET)
	{
		BOOL handled=FALSE;
		PacketHandler *ph;
		FOR_EACH_LINK(corehandlers,ph,l)
		{
			if(ph->type != subtype) continue;
			
			if(ph->func(dest,pkt,len))
			{
				handled=TRUE;
				// break; //ultimately not a good idea
			}
		}
		
		if(!handled) printf("unhandled core packet type on %d: %02x\n",dest,pkt[1]);
		else
		{
			// printf("recv core on %d: size=%d\n",dest,len);
			printpacket(pkt,len,dest,1,0);
		}
	}
	else
	{
		BOOL handled=FALSE;
		PacketHandler *ph;
		FOR_EACH_LINK(packethandlers,ph,l)
		{
			if(ph->type != type) continue;
			
			if(ph->func(dest,pkt,len))
			{
				handled=TRUE;
				// break; //ultimately not a good idea
			}
		}
		
		if(!handled) printf("unhandled packet type on %d: %02x\n",dest,pkt[0]);
		else
		{
			// printf("recv on %d: size=%d\n",dest,len);
			printpacket(pkt,len,dest,0,0);
		}
	}

}

local void* ReceiveThread(void *vp)
{
	while(TRUE)
	{
		SocketInfo* si=NULL;
		FOR_EACH_LINK(socketlist,si,l)
		{
			if(si->connected)
			{
				unsigned char rbuf[1024];
				memset(rbuf,0,sizeof(rbuf));
				int bytes=recv(si->socket,rbuf,sizeof(rbuf),0); //if set nonblocking, will return -1 on nothing there
				
				if(bytes != -1)
				{
					Byte *pkt=(Byte*)rbuf;
printf("recv on loc %i:\n",si->loc);
printpacket(pkt,bytes,si->loc,0,0);
					if(enc) enc->Decrypt(pkt,bytes);
					ReceivePacket(si->loc,pkt,bytes);
					{ DO_CBS(CB_PACKETARRIVED,PacketArrivedFunc,(si->loc,pkt,bytes)); }
				}
			}
		}

		fullsleep(5);
	}
}

local void* SendThread(void *vp)
{
	fullsleep(10);
}

local void RegisterEncryption(Iencrypt *ie)
{
	enc=ie; //last one loaded wins!
}

local void init(void)
{

#ifdef WIN32
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2,2),&wsaData) != NO_ERROR) lm->Log(L_WARN,"bad wsastartup: %d",WSAGetLastError());
#endif
	
	//create send and recieve threads
	pthread_create(&sthd,NULL,SendThread,NULL);
	pthread_create(&rthd,NULL,ReceiveThread,NULL);
}

local Inet netint=
{
	INTERFACE_HEAD_INIT(I_NET,"net"),
	CreateNewSocket, ConnectToSocket,
	AddPacket, RemovePacket, AddCorePacket, RemoveCorePacket,
	SendPacket, ReceivePacket,
	RegisterEncryption
};

EXPORT int MM_net(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		// enc=mm->GetInterface("enc-vie");
		enc=NULL; //to be gotten later
		if(!lm) return MM_FAIL;
		
		init();
		
		mm->RegInterface(&netint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&netint)) return MM_FAIL;
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}


