
#ifndef __NET_H
#define __NET_H

typedef enum
{
	LOC_NOWHERE		=0,
	LOC_CLIENT		=1,
	LOC_SERVER		=2,
	LOC_BILLER		=3,
	LOC_DIRECTORY	=4,
	LOC_REPOSITORY	=5,
	LOC_UPDATER		=6,
	LOC_PING		=7,
	LOC_OTHER		=8
} net_loc;

typedef int SocketID;

//exchange keys with a zone when called
#define CB_CONNECTIONINIT "connectioninit"
typedef void (*ConnectionInitFunc)(net_loc loc);

//for reporting uncaught packets
//also for handling packets that stupid people were too stupid to add a type byte to
#define CB_PACKETARRIVED "PacketArrivedFunc"
typedef void (*PacketArrivedFunc)(net_loc dest, void *pkt, int len);

//return TRUE for handled, FALSE for unhandled
typedef BOOL (*PacketFunc)(net_loc loc, Byte *data, int length);

#define I_NET "net-1"

typedef struct Inet
{
	INTERFACE_HEAD_DECL;

	//sockets
	SocketID (*CreateNewSocket)(net_loc loc, BOOL listenonly);
	BOOL (*ConnectToSocket)(SocketID id, char* ip, int port);
	
	//packet handlers
	void (*AddPacket)(net_loc loc, Byte type, PacketFunc func);
	void (*RemovePacket)(net_loc loc, Byte type, PacketFunc func);
	void (*AddCorePacket)(Byte type, PacketFunc func);
	void (*RemoveCorePacket)(Byte type, PacketFunc func);
	
	BOOL (*SendPacket)(net_loc loc, void *pkt, int len); //send packet normally
	void (*ReceivePacket)(net_loc loc, void *pkt, int len); //spoof recieving a packet
	
	void (*RegisterEncryption)(Iencrypt *enc);
	
} Inet;


#endif
