
#ifndef __PACKETS_H
#define __PACKETS_H

///----------------------------------------------------------------------------------------------------
//CORE PACKET TYPES
#define CORE_PACKET				0x00
#define CORE_LOGINREQUEST		0x01
#define CORE_LOGINRESPONSE		0x02
#define CORE_RELHEADER			0x03
#define CORE_RELACK				0x04 //apparently can send 0 to indicate no more data
#define CORE_SYNCREQUEST		0x05
#define CORE_SYNCRESPONSE		0x06
#define CORE_DISCONNECT			0x07
#define CORE_CHUNK				0x08
#define CORE_LASTCHUNK			0x09
#define CORE_STREAM				0x0A
#define CORE_STREAMCANCEL		0x0B
#define CORE_STREAMCANCELACK	0x0C
#define CORE_UNKNOWN			0x0D
#define CORE_CLUSTER			0x0E
#define CORE_UNKNOWN2			0x0F
#define CORE_ALTENCREQUEST		0x10
#define CORE_ALTENCRESPONSE		0x11

///----------------------------------------------------------------------------------------------------
//C2D PACKET TYPES
#define C2D_LISTREQUEST		0x01

typedef struct C2DListRequest
{
	u8 type; //0x01
	u32 minplayers;
} C2DListRequest;


//D2C PACKET TYPES
#define CORE_LOGINREQUEST		0x01

typedef struct C2DListData
{
	u8 type; //0x01
	u32 ip;
	u16 port;
	u16 players;
	u16 billing;
	u32 version;
	char name[64];
	u8 desc[];
} C2DListData;


///----------------------------------------------------------------------------------------------------
//S2D PACKET TYPES

//no type because apparently we are dumb
typedef struct S2DRegisterServer
{
	u32 ip;
	u16 port;
	u16 players;
	u16 scores;
	u32 version;
	char name[32];
	char password[32];
	char reserved[32];
	u8 desc[];
} S2DRegisterServer;


//D2S PACKET TYPES

//none yet

///----------------------------------------------------------------------------------------------------


//push current alignment to stack
#pragma pack(push,1)
//set alignment to 1 byte boundary
//#pragma pack(1)

///----------------------------------------------------------------------------------------------------


typedef struct CoreLoginRequest
{
	u8 type; //0x00
	u8 subtype; //0x01
	u32 key;
	u16 protocol; //vie = 0x01, cont = 0x11
} CoreLoginRequest;

typedef struct CoreLoginResponse
{
	u8 type; //0x00
	u8 subtype; //0x02
	u32 key; //negative
} CoreLoginResponse;

typedef struct CoreRelHeader
{
	u8 type; //0x00
	u8 subtype; //0x03
	u32 packetid;
	//payload
} CoreRelHeader;

typedef struct CoreRelAck
{
	u8 type; //0x00
	u8 subtype; //0x04
	u32 packetid;
} CoreRelAck;

typedef struct CoreSyncRequest
{
	u8 type; //0x00
	u8 subtype; //0x05
	u32 localtime;
	u32 packetssent;
	u32 packetsreceived;
} CoreSyncRequest;

typedef struct CoreSyncResponse
{
	u8 type; //0x00
	u8 subtype; //0x06
	u32 elapsedtime; //since last 0x05
	u32 servertime;
} CoreSyncResponse;

typedef struct CoreDisconnect
{
	u8 type; //0x00
	u8 subtype; //0x07
} CoreDisconnect;

typedef struct CoreChunk
{
	u8 type; //0x00
	u8 subtype; //0x08
	u8 data[];
} CoreChunk;

typedef struct CoreChunkEnd
{
	u8 type; //0x00
	u8 subtype; //0x09
	u8 data[];
} CoreChunkEnd;

typedef struct CoreStreamHeader
{
	u8 type; //0x00
	u8 subtype; //0x0A
	u32 totalsize;
	u8 data[];
} CoreStreamHeader;

typedef struct CoreStreamCancel
{
	u8 type; //0x00
	u8 subtype; //0x0B
} CoreStreamCancel;

typedef struct CoreStreamCancelAck
{
	u8 type; //0x00
	u8 subtype; //0x0C
} CoreStreamCancelAck;

typedef struct CoreClusterHeader
{
	u8 type; //0x00
	u8 subtype; //0x0E
	u8 size;
	u8 data[];
} CoreClusterHeader;

typedef struct CoreAltEncRequest
{
	u8 type; //0x00
	u8 subtype; //0x10
	u8 data[10];
} CoreAltEncRequest;

typedef struct CoreAltEncResponse
{
	u8 type; //0x00
	u8 subtype; //0x11
	u8 data[6];
} CoreAltEncResponse;


///----------------------------------------------------------------------------------------------------








///----------------------------------------------------------------------------------------------------



//restore original alignment from stack
#pragma pack(pop)

#endif















