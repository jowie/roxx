
#ifndef __SQL_H
#define __SQL_H


typedef struct db_res db_res;
typedef struct db_row db_row;

typedef void (*query_callback)(int status, db_res *res, void *clos);

#define I_SQL "sql"
typedef struct Isql
{
	INTERFACE_HEAD_DECL;

	int (*GetStatus)(); //0 if not connected, 1 if connected

	/* fmt may contain '?'s, which will be replaced by the corresponding
	 * argument as a properly escaped and quoted string, and '#'s, which
	 * will be replaced by the corresponding argument as an unsigned
	 * int. python users have to escape stuff themselves, using EscapeString. */
	int (*Query)(query_callback cb, void *clos, int notifyfail, const char *fmt, ...);

	int (*GetRowCount)(db_res *res);
	int (*GetFieldCount)(db_res *res);
	db_row* (*GetRow)(db_res *res);
	const char* (*GetField)(db_row *row, int fieldnum);

	/* returns the value generated for an AUTO_INCREMENT column for the
	 * previous INSERT or UPDATE. you must call this immediately after
	 * the query that you want the id from. if this returns -1, it means
	 * this database implementation doesn't support this functionality. */
	int (*GetLastInsertId)(void);

	//only useful to python. C users should use formatting characters in Query.
	int (*EscapeString)(const char *str, char *buf, int buflen);
} Isql;



#endif

