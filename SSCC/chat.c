
#include "ssc.h"
#include "player.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Igfx *gfx;
local Iplayer *pl;

local LinkedList lines=LL_INITIALIZER;

local bool menutoggle=FALSE;
local int resx=0;
local int resy=0;

typedef struct TextLine
{
	int color;
	char* text;
} TextLine;

local void Resolution(int x, int y)
{
	resx=x;
	resy=y;
}

local void RecieveChat(int color, char* text)
{	
	TextLine *tl=amalloc(sizeof(TextLine));

	tl->color=color;
	tl->text=strdup(text);

	LLAddLast(&lines,tl);
// printf("added: %s\n",text);
}

local void SendChat(ChatType type, int sound, int pid, char* text)
{
// printf("sending chat2: %s\n",text);
	int size=sizeof(C2SChat)+strlen(text)+1;
	C2SChat *c=amalloc(size);
	c->type=C2S_CHAT;
	c->kind=type;
	c->sound=sound;
	c->pid=pid;
	strcpy(c->text,text);
	net->SendPacket(DEST_ZONE,c,size);
	
	RecieveChat(type,text);
	// afree(c);
}

local bool handlecommand(char* cmd, char* text)
{
	//TODO: handle this
	return false;
}

local void HandleChat(char* text)
{
	if(!text) return;
	
	char* c=text;
	char* c2=text;
	switch(*text)
	{
		case '?': //command
			c++;
			c2=c;
			// while(*c && (*c != ' ') && (*c != '=')) c2++;
			c2++;
			if(!handlecommand(c,c2)) SendChat(CHAT_PUB,0,0,text);
		break;
		case ';': //chat
			c++;
			SendChat(CHAT_CHAT,0,0,c);
		break;
		case ':': //pm
			c++;
			c2=c;
			while(*c && (*c != ':')) c2++;
			*c2=0;
			c2++;
			SendChat(CHAT_RPRIV,0,0,text);
		break;
		case '/': //pm
			c++;
			SendChat(CHAT_PRIV,0,0,c);
		break;
		case '\'': //teamchat
			c++;
			SendChat(CHAT_TEAM,0,0,c);
			// SendChat(CHAT_PUB,0,0,text);
		break;
		case '"': //freq msg
			c++;
			SendChat(CHAT_FREQ,0,0,c);
		break;
		case '=': //change freq
			c++;
			// SendChat(CHAT_PUB,0,0,text);
			int freq=atoi(c);
			if(freq) DO_CBS(CB_FREQCHANGE,FreqChangeFunc,(freq));
		break;
		// case '\\': //staffchat
			// break;
		default: //pub
			SendChat(CHAT_PUB,0,0,text);
	}
}

local void ButtonPressed(Button key, bool down)
{
	if((key == BUTTON_MENU_MOD) && down) menutoggle=!menutoggle;
}

local void DrawGraphics(drawlayer layer)
{
	switch(layer)
	{
		case DL_CHAT:
		{
			int line=1;
			int maxline=5;
			if(menutoggle) maxline=40;
			int ph=0;
			TextLine *tl;
			FOR_EACH_LINK_REVERSE(lines,tl,l)
			{
				gfx->DrawTextAt(10,resy-40-ph,tl->color,8,12,tl->text);
				ph+=15;
				line++;
				if(line > maxline) break;
			}
		}
		break;
	}
}

local int packethandler(int dest, Byte *pkt, int len)
{
	if(dest != DEST_ZONE) return;
	
	switch(pkt[0])
	{
		case S2C_CHAT:
		{
			printf("s2c chat message type %d: %s\n",pkt[0],pkt+5);
			
			Player* p=NULL;
			char buf[1024];
			S2CChat* c=(S2CChat*)pkt;
			int color=CHAT_PUB;
			switch(c->kind)
			{
				case 0: //arena
				case 7: //remote pm
					RecieveChat(CHAT_ARENA,c->text);
				break;
				
				case 1: //pubm
				case 2: //pub
					p=pl->GetPlayer(c->pid);
					if(!p) break;
					sprintf(buf,"%10s> %s",p->name,c->text);
					RecieveChat(CHAT_PUB,buf);
				break;
				
				case 3: //team
				case 4: //freq
					p=pl->GetPlayer(c->pid);
					if(!p) break;
					sprintf(buf,"%10s> %s",p->name,c->text);
					RecieveChat(CHAT_TEAM,buf);
				break;
				
				case 5: //pm
					p=pl->GetPlayer(c->pid);
					if(!p) break;
					sprintf(buf,"%10s> %s",p->name,c->text);
					RecieveChat(CHAT_ARENA,buf);
				break;
				
				case 6: //server
				case 8: //server
					RecieveChat(CHAT_MOD,c->text);
				break;
				
				case 9: //chat
					RecieveChat(CHAT_CHAT,c->text);
				break;
				case 33: //first cont grey
					RecieveChat(CHAT_GREY,c->text);
				break;
				case 44: //second cont grey that displays ingame, now used here for the pink that cont can not display
					RecieveChat(CHAT_PINK,c->text);
				break;
				case 79: //purple
					RecieveChat(CHAT_PURPLE,c->text);
				break;
				default:
					RecieveChat(CHAT_GREY,c->text);
			}
			
			// DO_CBS(CB_CHATRECIEVED,ChatRecievedFunc,(pkt[0],pkt+5));
			
			return TRUE;
		}
		break;
	}

	return FALSE;
}

local Ichat chatint=
{
	INTERFACE_HEAD_INIT(I_CHAT,"chat"),
	SendChat, RecieveChat, HandleChat
};

EXPORT int MM_chat(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		gfx=mm->GetInterface(I_GFX);
		pl=mm->GetInterface(I_PLAYER);
		if(!lm || !net || !gfx || !pl) return MM_FAIL;
		
		net->AddPacket(S2C_CHAT,packethandler);
		
		// mm->RegCallback(CB_CHATRECIEVED,ChatRecieved);
		mm->RegCallback(CB_DRAWGRAPHICS,DrawGraphics);
		mm->RegCallback(CB_BUTTONPRESSED,ButtonPressed);
		mm->RegCallback(CB_RESOLUTION,Resolution);
		
		mm->RegInterface(&chatint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&chatint)) return MM_FAIL;
		
		// mm->UnregCallback(CB_CHATRECIEVED,ChatRecieved);
		mm->UnregCallback(CB_DRAWGRAPHICS,DrawGraphics);
		mm->UnregCallback(CB_BUTTONPRESSED,ButtonPressed);
		mm->UnregCallback(CB_RESOLUTION,Resolution);
		
		net->RemovePacket(S2C_CHAT,packethandler);
		
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(gfx);
		mm->ReleaseInterface(pl);
		
		return MM_OK;
	}
	return MM_FAIL;
}

