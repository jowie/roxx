
#ifndef __CORE_H
#define __CORE_H

//called when zone wants to disconnect
#define CB_ZONEDISCONNECT "zonedisconnect"
typedef void (*ZoneDisconnectFunc)(void);

#endif

