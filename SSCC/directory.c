
#include "ssc.h"

local Imodman *mm;
local Inet *net;

local void Login(void)
{
	//sscentral.sscuservers.net=195.250.184.251
	//ssdir.playsubspace.com=5.2.16.136
	DO_CBS(CB_CONNECTIONCONNECT,ConnectionConnectFunc,(DEST_DIR,"195.250.184.251",4990));
	// DO_CBS(CB_CONNECTIONCONNECT,ConnectionConnectFunc,(DEST_DIR,"5.2.16.136",4990));
}

local void ConnectionReady(int dest)
{
	C2DListRequest lr;
	int size=sizeof(C2DListRequest);
	lr.type=C2D_LISTREQUEST;
	lr.minimumplayers=10;
	net->SendPacket(DEST_DIR,&lr,size);
}

local void GetList(void)
{
	Login();
}

local int packethandler(int dest, Byte *pkt, int len)
{
	if(dest != DEST_DIR) return;
	
	switch(pkt[0])
	{
		case D2C_LISTRESPONSE:
		{
			printf("directory list data recieved: size=%d\n",len);
			
			D2CListResponse *lr=(D2CListResponse*)pkt;
			int entries=len/sizeof(ListEntry);
			ListEntry *le=&(lr->le);
			int c=1;
			while(c <= entries)
			{
				printf("directory zone %d of %d: ip=%d port=%d players=%d billing=%d version=%d name=%s description=%s\n",c,entries,
le->ip,
le->port,
le->players,
le->billing,
le->version,
le->name,
le->description);

Byte *bp=(Byte*)&(le->description);
while(*bp) bp++;
bp++;
le=(ListEntry*)bp;
			
				c++;
			}
			
			
			CoreDisconnect cd;
			cd.type=CORE_PACKET;
			cd.subtype=CORE_DISCONNECT;
			net->SendPacket(DEST_DIR,&cd,sizeof(CoreDisconnect));
		}
		break;
	}
}

local Idirectory idir=
{
	INTERFACE_HEAD_INIT(I_DIRECTORY,"directory"),
	GetList
};

EXPORT int MM_directory(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		net=mm->GetInterface(I_NET);
		if(!net) return MM_FAIL;
		
		net->AddPacket(D2C_LISTRESPONSE,packethandler);
		// net->AddCorePacket(CORE_LOGINRESPONSE,packethandler);
		mm->RegCallback(CB_CONNECTIONREADY,ConnectionReady);
		
		mm->RegInterface(&idir);
		
		// GetList();
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&idir)) return MM_FAIL;
		
		mm->UnregCallback(CB_CONNECTIONREADY,ConnectionReady);
		// net->RemoveCorePacket(CORE_LOGINRESPONSE,packethandler);
		net->RemovePacket(D2C_LISTRESPONSE,packethandler);
		
		mm->ReleaseInterface(net);
		
		return MM_OK;
	}
	return MM_FAIL;
}
