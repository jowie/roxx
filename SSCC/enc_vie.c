
#include "ssc.h"

local Imodman *mm;
local Inet *net;

typedef struct EncryptData
{
	int key;
	char table[520];
} EncryptData;

#define BAD_KEY (-1)
EncryptData *ed=NULL;

local void do_init(int k)
{
	ed->key=k;
	if(k == 0) return;

	short *mytable=(short*)(ed->table);

	int t;
	int loop;
	for(loop=0; loop < 0x104; loop++)
	{
		t=((i64)k * 0x834E0B5F) >> 48;
		t += t >> 31;
		k=((k % 127773) * 16807) - (t * 2836) + 123;
		if(!k || (k & 0x80000000)) k += 0x7FFFFFFF;
		mytable[loop]=(short)k;
	}
}

local int do_enc(Byte *data, int len)
{
	int work=ed->key;
	int *mytable=(int*)ed->table;
	int until;
	int *mydata;

	if(work == 0 || mytable == NULL) return len;

	if(data[0] == 0)
	{
		mydata=(int*)(data + 2);
		until=(len-2)/4 + 1;
	}
	else
	{
		mydata=(int*)(data + 1);
		until=(len-1)/4 + 1;
	}

	int loop;
	for(loop=0; loop < until; loop++)
	{
		work=mydata[loop] ^ (mytable[loop] ^ work);
		mydata[loop]=work;
	}
	return len;
}

local int do_dec(Byte *data, int len)
{
	int work=ed->key, *mytable=(int*)ed->table;
	int *mydata, loop, until;

	if(work == 0 || mytable == NULL) return len;

	if(data[0] == 0)
	{
		mydata=(int*)(data + 2);
		until=(len-2)/4 + 1;
	}
	else
	{
		mydata=(int*)(data + 1);
		until=(len-1)/4 + 1;
	}

	for (loop=0; loop < until; loop++)
	{
		int tmp=mydata[loop];
		mydata[loop]=mytable[loop] ^ work ^ tmp;
		work=tmp;
	}
	return len;
}


local int Encrypt(Byte *d, int n)
{
	if((d[0] == 0x00) && (d[1] == 0x01))
	{
		/* sending key init */
		/* temporarily overload the key field to be what _we_ sent */
		ed->key=*(int*)(d+2);
		return n;
	}
	else if(ed->key != BAD_KEY) return do_enc(d,n);
	else return n;
}

local int Decrypt(Byte *d, int n)
{
	if((d[0] == 0x00) && (d[1] == 0x02))
	{
		/* got key response */
		int gotkey=*(int*)(d+2);
		if(gotkey == ed->key) ed->key=BAD_KEY; // signal for no encryption
		else do_init(gotkey);
		return n;
	}
	else if(ed->key != BAD_KEY) return do_dec(d,n);
	else return n;
}

local void ConnectionInit(int dest)
{
	CoreLoginRequest cl;
	cl.type=CORE_PACKET;
	cl.subtype=CORE_LOGINREQUEST;
	cl.key=-123; //should random it for vie
	// cl.key=-217254918;
	cl.protocol=0x01;
	
	net->SendPacket(dest,&cl,sizeof(CoreLoginRequest));
}

local int packethandler(int dest, Byte *pkt, int len)
{
	switch(pkt[0])
	{
		case CORE_PACKET:
		{
			switch(pkt[1])
			{
				case CORE_LOGINRESPONSE:
				{
					CoreLoginResponse *clr=(CoreLoginResponse*)pkt;
					printf("login response key=%i ~key=%i\n",clr->key,~clr->key);
					
					DO_CBS(CB_CONNECTIONREADY,ConnectionReadyFunc,(dest));
					
					return TRUE;
				}
				break;
			}
		}
	}
}

local Iencrypt ienc=
{
	INTERFACE_HEAD_INIT("enc-vie","enc-vie"),
	Encrypt, Decrypt
};

EXPORT int MM_enc_vie(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		net=mm->GetInterface(I_NET);
		if(!net) return MM_FAIL;
		
		ed=amalloc(sizeof(EncryptData));
		ed->key=BAD_KEY;
		net->RegisterEncryption(&ienc);
		
		net->AddCorePacket(CORE_LOGINRESPONSE,packethandler);
		mm->RegCallback(CB_CONNECTIONINIT,ConnectionInit);
		
		mm->RegInterface(&ienc);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&ienc)) return MM_FAIL;
		
		mm->UnregCallback(CB_CONNECTIONINIT,ConnectionInit);
		net->RemoveCorePacket(CORE_LOGINRESPONSE,packethandler);
		
		afree(ed);
		
		mm->ReleaseInterface(net);
		
		return MM_OK;
	}
	return MM_FAIL;
}


