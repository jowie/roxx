
#include <stdio.h>

#include "defs.h"
#include "module.h"
#include "logman.h"

local Imodman *mm;
local Ilogman *lm;

local void LogConsole(char *s)
{
	puts(s);
}

EXPORT int MM_log_console(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		if(!lm) return MM_FAIL;
		
		mm->RegCallback(CB_LOGFUNC,LogConsole);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		mm->UnregCallback(CB_LOGFUNC,LogConsole);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

