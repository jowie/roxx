
#ifndef __NET_H
#define __NET_H

#define DEST_NOWHERE	0
#define DEST_ZONE		1
#define DEST_DIR		2
#define DEST_PING		3

//call this to connect to a zone
#define CB_CONNECTIONCONNECT "connectionconnect"
typedef void (*ConnectionConnectFunc)(int dest, char *ip, int port);

//exchange keys with a zone when called
#define CB_CONNECTIONINIT "connectioninit"
typedef void (*ConnectionInitFunc)(int dest);

//return TRUE for handled, FALSE for unhandled
typedef BOOL (*PacketFunc)(int dest, Byte *data, int length);

#define I_NET "net-1"

typedef struct Inet
{
	INTERFACE_HEAD_DECL;

	void (*AddPacket)(Byte type, PacketFunc func);
	void (*RemovePacket)(Byte type, PacketFunc func);
	void (*AddCorePacket)(Byte type, PacketFunc func);
	void (*RemoveCorePacket)(Byte type, PacketFunc func);
	
	void (*SendPacket)(int dest, void *pkt, int len);
	void (*ReceivePacket)(int dest, void *pkt, int len);
	
	void (*RegisterEncryption)(Iencrypt *enc);
	
} Inet;


#endif
