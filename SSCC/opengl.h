

#ifndef __OPENGL_H
#define __OPENGL_H

typedef enum
{
// BelowAll,AfterBackground,AfterTiles,AfterWeapons,
// AfterShips,AfterGauges,AfterChat,TopMost
	DL_BELOWALL=0,
	DL_BG,
	DL_AFTERBG,
	DL_TILES,
	DL_AFTERTILES,
	DL_WEAPONS,
	DL_AFTERWEAPONS,
	DL_SHIPS,
	DL_AFTERSHIPS,
	DL_HUD,
	DL_AFTERHUD,
	DL_CHAT,
	DL_AFTERCHAT,
	DL_TOPMOST
} drawlayer;



typedef struct Igfx
{
	INTERFACE_HEAD_DECL;

	void (*DrawTextAt)(int x, int y, ChatType color, int sizex, int sizey, char* text);
	void (*DrawPlayerShip)(Player *p);
	
} Igfx;

#define I_GFX "gfx-1"

#define CB_CREATEGRAPHICS "creategraphics"
typedef void (*CreateGraphicsFunc)(void);

#define CB_DRAWGRAPHICS "drawgraphics"
typedef void (*DrawGraphicsFunc)(drawlayer layer);

#define CB_DESTROYGRAPHICS "destroygraphics"
typedef void (*DestroyGraphicsFunc)(void);

#define I_OPENGL "opengl-1"

typedef struct Iopengl
{
	INTERFACE_HEAD_DECL;

	void (*StartDrawing)(HWND hwnd, HDC *hdc, HGLRC *hrc);
	void (*StopDrawing)(HWND hwnd, HDC hdc, HGLRC hrc);
	void (*SetResolution)(int x, int y);
	void (*CreateGraphic)(char* filename, int* id, int* width, int* height, BOOL transparent);
	void (*DrawSprite)(int id, int x, int y, int w, int h, int col, int row, int maxcols, int maxrows);
	void (*Translate)(int x, int y);
	void (*Scale)(double n);
	void (*Rotate)(double n, int x, int y);
	
} Iopengl;


#endif

