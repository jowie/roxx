
#ifndef __PACKETS_H
#define __PACKETS_H

///----------------------------------------------------------------------------------------------------
//CORE PACKET TYPES
#define CORE_PACKET				0x00
#define CORE_LOGINREQUEST		0x01
#define CORE_LOGINRESPONSE		0x02
#define CORE_RELHEADER			0x03
#define CORE_RELACK				0x04 //apparently can send 0 to indicate no more data
#define CORE_SYNCREQUEST		0x05
#define CORE_SYNCRESPONSE		0x06
#define CORE_DISCONNECT			0x07
#define CORE_CHUNK				0x08
#define CORE_LASTCHUNK			0x09
#define CORE_STREAM				0x0A
#define CORE_STREAMCANCEL		0x0B
#define CORE_STREAMCANCELACK	0x0C
#define CORE_UNKNOWN			0x0D
#define CORE_CLUSTER			0x0E
#define CORE_UNKNOWN2			0x0F
#define CORE_ALTENCREQUEST		0x10
#define CORE_ALTENCRESPONSE		0x11

///----------------------------------------------------------------------------------------------------
//C2S PACKET TYPES
#define C2S_CORE				0x00
#define C2S_ARENACHANGE			0x01
#define C2S_LEAVING				0x02
#define C2S_POSITION			0x03
#define C2S_NOOP1				0x04 //Packet tampering?
#define C2S_DIE					0x05 //Packet tampering?
#define C2S_CHAT				0x06
#define C2S_GREEN				0x07
#define C2S_SPECREQUEST			0x08
#define C2S_LOGINREQUEST		0x09
#define C2S_REBROADCAST			0x0A //Packet tampering?
#define C2S_UPDATEREQUEST		0x0B
#define C2S_MAPREQUEST			0x0C
#define C2S_NEWSREQUEST			0x0D
#define C2S_RELAYVOICE			0x0E
#define C2S_SETFREQ				0x0F
#define C2S_ATTACHTO			0x10
#define C2S_NOOP2				0x11 //Packet tampering?
#define C2S_NOOP3				0x12 //Packet tampering?
#define C2S_PICKUPFLAG			0x13
#define C2S_TURRETKICKOFF		0x14
#define C2S_DROPFLAGS			0x15
#define C2S_UPLOADFILE			0x16 //uploading a file to server
#define C2S_REGDATA				0x17
#define C2S_SETSHIP				0x18
#define C2S_BANNER				0x19 //sending new banner
#define C2S_SECURITYRESPONSE	0x1A
#define C2S_CHECKSUMMISMATCH	0x1B
#define C2S_BRICK				0x1C
#define C2S_SETTINGCHANGE		0x1D
#define C2S_KOTHEXPIRED			0x1E
#define C2S_SHOOTBALL			0x1F
#define C2S_PICKUPBALL			0x20
#define C2S_GOAL				0x21
#define C2S_NOOP4				0x22 //missing 22 : subspace client sends extra checksums and other security stuff
#define C2S_NOOP5				0x23
//apparently 0x23-0x2B have to do with continuum login
#define C2S_CONTLOGIN			0x24
#define C2S_DAMAGE				0x32

///----------------------------------------------------------------------------------------------------
//S2C PACKET TYPES
#define S2C_CORE				0x00
#define S2C_YOURPID				0x01
#define S2C_ENTERINGARENA		0x02
#define S2C_PLAYERENTERING		0x03
#define S2C_PLAYERLEAVING		0x04
#define S2C_WEAPON				0x05
#define S2C_KILL				0x06
#define S2C_CHAT				0x07
#define S2C_GREEN				0x08
#define S2C_SCOREUPDATE			0x09
#define S2C_LOGINRESPONSE		0x0A
#define S2C_SOCCERGOAL			0x0B
#define S2C_VOICE				0x0C
#define S2C_FREQCHANGE			0x0D
#define S2C_TURRET				0x0E
#define S2C_SETTINGS			0x0F
#define S2C_INCOMINGFILE		0x10
#define S2C_NOOP1				0x11
#define S2C_FLAGLOC				0x12
#define S2C_FLAGPICKUP			0x13
#define S2C_FLAGRESET			0x14
#define S2C_TURRETKICKOFF		0x15
#define S2C_FLAGDROP			0x16
#define S2C_NOOP2				0x17
#define S2C_SECURITY			0x18 //Synchronization?
#define S2C_REQUESTFORFILE		0x19
#define S2C_TIMEDGAME			0x1A
#define S2C_SHIPRESET			0x1B //just 1 byte, tells client they need to reset their ship
#define S2C_SPECDATA			0x1C /* two bytes, if byte two is true, client needs to send their item info in position packets, OR
										* three bytes, parameter is the player id of a player going into spectator mode */
#define S2C_SHIPCHANGE			0x1D
#define S2C_BANNERTOGGLE		0x1E
#define S2C_BANNER				0x1F
#define S2C_PRIZERECV			0x20
#define S2C_BRICK				0x21
#define S2C_TURFFLAGS			0x22
#define S2C_PERIODICREWARD		0x23
#define S2C_SPEED				0x24 //complex speed stats
#define S2C_UFO					0x25 //two bytes, if byte two is true, you can use UFO if you want to
#define S2C_NOOP3				0x26
#define S2C_KEEPALIVE			0x27
#define S2C_POSITION			0x28
#define S2C_MAPFILENAME			0x29
#define S2C_MAPDATA				0x2A
#define S2C_SETKOTHTIMER		0x2B
#define S2C_KOTH				0x2C
#define S2C_NOOP4				0x2D //missing 2D : some timer change?
#define S2C_BALL				0x2E
#define S2C_ARENA				0x2F
#define S2C_ADBANNER			0x30 //vie's old method of showing ads
#define S2C_LOGINOK				0x31 //vie sent it after a good login, only with billing
#define S2C_WARPTO				0x32 //u8 type - ui16 x tile coords - ui16 y tile coords
#define S2C_LOGINTEXT			0x33
#define S2C_CONTVERSION			0x34
#define S2C_TOGGLEOBJ			0x35 //u8 type - unlimited number of ui16 with obj id (if & 0xF000, means turning off)
#define S2C_MOVEOBJECT			0x36
#define S2C_TOGGLEDAMAGE		0x37 //two bytes, if byte two is true, client should send damage info
#define S2C_DAMAGE				0x38 //complex, the info used from a *watchdamage
#define S2C_NOOP5				0x39
#define S2C_NOOP6				0x3A
#define S2C_REDIRECT			0x3B


//push current alignment to stack
#pragma pack(push,1)
//set alignment to 1 byte boundary
//#pragma pack(1)

///----------------------------------------------------------------------------------------------------


typedef struct CoreLoginRequest
{
	u8 type; //0x00
	u8 subtype; //0x01
	u32 key;
	u16 protocol; //vie = 0x01, cont = 0x11
} CoreLoginRequest;

typedef struct CoreLoginResponse
{
	u8 type; //0x00
	u8 subtype; //0x02
	u32 key; //negative
} CoreLoginResponse;

typedef struct CoreRelHeader
{
	u8 type; //0x00
	u8 subtype; //0x03
	u32 packetid;
	//payload
} CoreRelHeader;

typedef struct CoreRelAck
{
	u8 type; //0x00
	u8 subtype; //0x04
	u32 packetid;
} CoreRelAck;

typedef struct CoreSyncRequest
{
	u8 type; //0x00
	u8 subtype; //0x05
	u32 localtime;
	u32 packetssent;
	u32 packetsreceived;
} CoreSyncRequest;

typedef struct CoreSyncResponse
{
	u8 type; //0x00
	u8 subtype; //0x06
	u32 elapsedtime; //since last 0x05
	u32 servertime;
} CoreSyncResponse;

typedef struct CoreDisconnect
{
	u8 type; //0x00
	u8 subtype; //0x07
} CoreDisconnect;

typedef struct CoreChunk
{
	u8 type; //0x00
	u8 subtype; //0x08
	u8 data[];
} CoreChunk;

typedef struct CoreChunkEnd
{
	u8 type; //0x00
	u8 subtype; //0x09
	u8 data[];
} CoreChunkEnd;

typedef struct CoreStreamHeader
{
	u8 type; //0x00
	u8 subtype; //0x0A
	u32 totalsize;
	u8 data[];
} CoreStreamHeader;

typedef struct CoreStreamCancel
{
	u8 type; //0x00
	u8 subtype; //0x0B
} CoreStreamCancel;

typedef struct CoreStreamCancelAck
{
	u8 type; //0x00
	u8 subtype; //0x0C
} CoreStreamCancelAck;

typedef struct CoreClusterHeader
{
	u8 type; //0x00
	u8 subtype; //0x0E
	u8 size;
	u8 data[];
} CoreClusterHeader;

typedef struct CoreAltEncRequest
{
	u8 type; //0x00
	u8 subtype; //0x10
	u8 data[10];
} CoreAltEncRequest;

typedef struct CoreAltEncResponse
{
	u8 type; //0x00
	u8 subtype; //0x11
	u8 data[6];
} CoreAltEncResponse;


///----------------------------------------------------------------------------------------------------


typedef struct C2SPingRequest
{
	u32 timestamp;
} C2SPingRequest;

typedef struct S2CPingResponse
{
	u32 total;
	u32 timestamp;
} S2CPingResponse;



typedef struct C2SLoginRequest //101+64=165 bytes
{
	u8 type;  //0x09
	u8 flags; //newuser 1=yes 0=no
	u8 name[32];
	u8 password[32];
	u32 macid; //C: serial number?
	i8 connecttype; //0
	u16 timezonebias;
	u16 unknown1; //0x6F9D? cversion?
	i16 version; //134?
	i32 unknown2; //444
	i32 unknown3; //555
	u32 permissionid; //# in registry? D2?
	i8 unknown4[12]; //0
	// u8 contid[64];
} C2SLoginRequest;


typedef struct S2CLoginResponse
{
	u8 type;
	u8 response;
	u32 version;
	u8 isvip;
	u8 unknown1;
	u8 unknown2;
	u8 unknown3;
	u32 exechecksum;
	u32 unknown4;
	u8 unknown5;
	u8 registrationrequest; //demodata?
	u32 codechecksum;
	u32 newschecksum;
	u32 unknown6;
	u32 unknown7;
} S2CLoginResponse;



///----------------------------------------------------------------------------------------------------













///----------------------------------------------------------------------------------------------------



//weapon codes
#define W_NULL          0
#define W_BULLET        1
#define W_BOUNCEBULLET  2
#define W_BOMB          3
#define W_PROXBOMB      4
#define W_REPEL         5
#define W_DECOY         6
#define W_BURST         7
#define W_THOR          8
#define W_WORMHOLE      0 //used in watchdamage packet only

typedef struct Weapons //2 bytes
{
	u16 type			:5;
	u16 level			:2;
	u16 shrapbouncing	:1;
	u16 shraplevel		:2;
	u16 shrap			:5;
	u16 alternate		:1;
} Weapons;


typedef struct ExtraPosData //10 bytes
{
	u16 energy;
	u16 s2cping;
	u16 timer;
	u32 shields		:1;
	u32 super		:1;
	u32 bursts		:4;
	u32 repels		:4;
	u32 thors		:4;
	u32 bricks		:4;
	u32 decoys		:4;
	u32 rockets		:4;
	u32 portals		:4;
	u32 padding		:2;
} ExtraPosData;


typedef struct S2CWeapons //19+2+10=31 bytes
{
	u8 type; //0x05
	i8 rotation;
	u16 time;
	i16 x;
	i16 yspeed;
	u16 pid;
	i16 xspeed;
	u8 checksum;
	u8 status;
	u8 c2slatency;
	i16 y;
	u16 bounty;
	Weapons weapon;
	ExtraPosData extra;
} S2CWeapons;


typedef struct S2CPosition //16+2=18 bytes
{
	u8 type; //0x28
	i8 rotation;
	u16 time;
	i16 x;
	u8 c2slatency;
	u8 bounty;
	u8 pid;
	u8 status;
	i16 yspeed;
	i16 y;
	i16 xspeed;
	ExtraPosData extra;
} S2CPosition;


typedef struct C2SPosition //20+2+10=32 bytes
{
	u8 type; //0x03
	i8 rotation;
	u32 time;
	i16 xspeed;
	i16 y;
	u8 checksum;
	u8 status;
	i16 x;
	i16 yspeed;
	u16 bounty;
	i16 energy;
	Weapons weapon;
	ExtraPosData extra;
} C2SPosition;



///----------------------------------------------------------------------------------------------------




typedef struct C2SArenaChange //10+16+1=27 bytes
{
	u8 type;
	u8 shiptype;
	i8 noobscenity;
	i8 wavmsg;
	i16 xres;
	i16 yres;
	i16 arenanum;
	char arenaname[16];
	u8 optionalgraphics; //cont 
} C2SArenaChange;


typedef struct C2SShipChange //2 bytes
{
	u8 type; //0x18
	u8 ship; //0=wb 8=spec
} C2SShipChange;

typedef struct C2SFreqChange //2 bytes
{
	u8 type; //0x0F
	u16 freq;
} C2SFreqChange;



///----------------------------------------------------------------------------------------------------

typedef struct S2CChat
{
	u8 type; //0x07
	u8 kind;
	u8 sound;
	u16 pid;
	char text[];
} S2CChat;

typedef struct C2SChat
{
	u8 type; //0x06
	u8 kind;
	u8 sound;
	u16 pid;
	char text[];
} C2SChat;

typedef struct S2CPlayerEntering //64 bytes
{
	u8 type;  //0x03
	u8 ship;
	u8 aam;
	u8 name[20];
	u8 squad[20];
	u32 fpoints;
	u32 kpoints;
	u16 pid;
	u16 freq;
	u16 kills;
	u16 deaths;
	u16 turretee;
	u16 flags;
	u8 koth;
} S2CPlayerEntering;

typedef struct S2CPlayerLeaving //2 bytes
{
	u8 type; //0x04
	u8 pid;
} S2CPlayerLeaving;



typedef struct S2CKill
{
	u8 type; //0x18
	u8 green;
	u16 killer;
	u16 killed;
	u16 bounty;
	u16 flags;
} S2CKill;

typedef struct S2CFreqChange
{
	u8 type; //0x0F
	u16 pid;
	u16 freq;
	u8 unknown;
} S2CFreqChange;

typedef struct S2CShipChange
{
	u8 type; //0x1D
	u8 ship;
	u16 pid;
	u16 freq;
} S2CShipChange;


///----------------------------------------------------------------------------------------------------


typedef struct MiscBitfield /* 2 bytes */
{
	u16 SeeBombLevel   : 2;
	u16 DisableFastShooting : 1;
	u16 Radius         : 8;
	u16 _padding       : 5;
} MiscBitfield;

typedef struct ShipSettings /* 144 bytes */
{
	i32 long_set[2];
	i16 short_set[49];
	i8 byte_set[18];
	
	u32 ShrapnelMax    : 5;
	u32 ShrapnelRate   : 5;
	u32 CloakStatus    : 2;
	u32 StealthStatus  : 2;
	u32 XRadarStatus   : 2;
	u32 AntiWarpStatus : 2;
	u32 InitialGuns    : 2;
	u32 MaxGuns        : 2;
	u32 InitialBombs   : 2;
	u32 MaxBombs       : 2;
	u32 DoubleBarrel   : 1;
	u32 EmpBomb        : 1;
	u32 SeeMines       : 1;
	u32 Unused1        : 3;
	
	byte Padding[16];
} ShipSettings;


typedef struct ClientSettings
{
	u32 type : 8; /* 0x0F */
	u32 ExactDamage : 1;
	u32 HideFlags : 1;
	u32 NoXRadar : 1;
	u32 SlowFrameRate : 3;
	u32 DisableScreenshot : 1;
	u32 _reserved : 1;
	u32 MaxTimerDrift : 3;
	u32 DisableBallThroughWalls : 1;
	u32 DisableBallKilling : 1;
	u32 _padding : 11;
	
	
	ShipSettings ships[8];
	i32 long_set[20];
	struct /* 4 bytes */
	{
		u32 x : 10;
		u32 y : 10;
		u32 r : 9;
		u32 pad : 3;
	} spawn_pos[4];
	i16 short_set[58];
	i8 byte_set[32];
	u8 prizeweight_set[28];
};

///----------------------------------------------------------------------------------------------------



//restore original alignment from stack
#pragma pack(pop)

#endif















