

#ifndef __PARSEBMP_H
#define __PARSEBMP_H


#pragma pack(push,1)
typedef struct BGRPixel
{
	u8 b;
	u8 g;
	u8 r;
} BGRPixel;

typedef struct BMPFH
{
	u16 magic; //0
	u32 size; //2
	u16 unused1; //6
	u16 unused2; //8
	u32 offset; //10
	u32 headersize; //14
	u32 width; //18
	u32 height; //22
	u16 colorplanes; //26
	u16 bitsperpixel; //28
	u32 compression; //30
	u32 datasize; //34
	u32 xres; //38
	u32 vres; //42
	u32 numcolors; //46
	u32 colortype; //50
	BGRPixel data[];
} BMPFH;
#pragma pack(pop)


#define I_PARSEBMP "parsebmp-1"

typedef struct Iparsebmp
{
	INTERFACE_HEAD_DECL;

	unsigned char* (*ParseBMP)(char* filename, int *width, int *height);
	
} Iparsebmp;


#endif

