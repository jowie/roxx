
#include "ssc.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Igfx *gfx;
local Ichat *chat=NULL;

local LinkedList players=LL_INITIALIZER;

local Player* GetPlayer(int pid)
{
	Player *p=NULL;
	FOR_EACH_LINK(players,p,l)
	{
		if(p->pid == pid) return p;
	}
	return p;
}

local Player* GetPlayerByName(char* name)
{
	Player *p=NULL;
	FOR_EACH_LINK(players,p,l)
	{
		if(!strcmpi(p->name,name)) return p;
	}
	return p;
}

local int Player2PID(Player *p)
{
	Player *pp=NULL;
	FOR_EACH_LINK(players,pp,l)
	{
		if(pp == p) return p->pid;
	}
	return 0;
}

local int Name2PID(char* name)
{
	Player *p=NULL;
	FOR_EACH_LINK(players,p,l)
	{
		if(!strcmpi(p->name,name)) return p->pid;
	}
	return 0;
}

local void DrawGraphics(drawlayer layer)
{
	switch(layer)
	{
		case DL_SHIPS:
		{
			char buf[512];
			Player *p;
			FOR_EACH_LINK(players,p,l)
			{
				if(p->ship == SHIP_SPEC) continue;
				gfx->DrawPlayerShip(p);
				
				int color=CHAT_PUB;
				if(p->freq == self.freq) color=CHAT_TEAM;
				sprintf(buf,"%s (%d:%d)",p->name,p->ship,p->Ship.bounty,p->Ship.flags);
				gfx->DrawTextAt(p->Ship.x+40,p->Ship.y,color,8,12,buf);
				
				sprintf(buf,"%d",p->Ship.energy);
				gfx->DrawTextAt(p->Ship.x+40,p->Ship.y+30,CHAT_GREY,8,12,buf);
			}
		}
		break;
		case DL_CHAT:
		{
			int y=0;
			char buf[512];
			Player *p;
			FOR_EACH_LINK(players,p,l)
			{
				sprintf(buf,"%s(%i:%i, [%i/%i])",p->name,p->freq,p->ship,p->Score.kills,p->Score.deaths);
				gfx->DrawTextAt(1500,10+y,CHAT_ARENA,8,12,buf);
				y+=15;
			}
		}
		break;
	}
}

local int packethandler(int dest, Byte *pkt, int len)
{
	if(dest != DEST_ZONE) return;
	
	if(!chat) chat=mm->GetInterface(I_CHAT); //because fuck circular dependency shit
	if(!chat) printf("chat module missing for player module, shit is fucked up yo\n");
	
	switch(pkt[0])
	{
		case S2C_ENTERINGARENA:
		{
			printf("s2c type %d: entering arena\n",pkt[0]);
			
			chat->RecieveChat(CHAT_GREY,"You are entering the arena");
			
			return TRUE;
		}
		break;
		case S2C_PLAYERENTERING:
		{
			printf("s2c type %d: player entered\n",pkt[0]);
			
			S2CPlayerEntering* pe=(S2CPlayerEntering*)pkt;
			Player* p=amalloc(sizeof(Player));
			// p->=pe->type;
			p->ship=pe->ship;
			p->aam=pe->aam;
			strcpy(p->name,pe->name);
			strcpy(p->squad,pe->squad);
			p->Score.fpoints=pe->fpoints;
			p->Score.kpoints=pe->kpoints;
			p->pid=pe->pid;
			p->freq=pe->freq;
			p->Score.kills=pe->kills;
			p->Score.deaths=pe->deaths;
			p->turretee=pe->turretee;
			p->Ship.flags=pe->flags;
			p->koth=pe->koth;
			
			LLAddLast(&players,p);
			
			char buf[1024];
			sprintf(buf,"%s entered arena",p->name);
			chat->RecieveChat(CHAT_GREY,buf);
			
			return TRUE;
		}
		break;
		case S2C_PLAYERLEAVING:
		{
			printf("s2c type %d: player left\n",pkt[0]);
			
			S2CPlayerLeaving* pl=(S2CPlayerLeaving*)pkt;
			int pid=pl->pid;
			
			Player *p=GetPlayer(pl->pid);
			if(p)
			{
				char buf[1024];
				sprintf(buf,"%s left arena",p->name);
				chat->RecieveChat(CHAT_GREY,buf);
		
				LLRemove(&players,p);
				afree(p);
			}
			
			return TRUE;
		}
		break;
		case S2C_KILL:
		{
			printf("s2c type %d: kill\n",pkt[0]);
			
			S2CKill* k=(S2CKill*)pkt;
//k->green;
			Player *killer=GetPlayer(k->killer);
			Player *killed=GetPlayer(k->killed);
			
			char buf[1024];
			sprintf(buf,"%s killed %s(%i:%i)",killer->name,killed->name,k->bounty,k->flags);
			chat->RecieveChat(CHAT_GREY,buf);
			
			killer->Score.kills++;
			killed->Score.deaths++;
			
			return TRUE;
		}
		break;
		case S2C_FREQCHANGE:
		{
			printf("s2c type %d: freq change\n",pkt[0]);
			
			S2CFreqChange* fc=(S2CFreqChange*)pkt;
// fc->unknown;
			Player *p=GetPlayer(fc->pid);
			
			char buf[1024];
			sprintf(buf,"%s changed to freq %i",p->name,fc->freq);
			chat->RecieveChat(CHAT_GREY,buf);
			
			p->freq=fc->freq;
			
			return TRUE;
		}
		break;
		case S2C_SHIPCHANGE:
		{
			printf("s2c type %d: ship change\n",pkt[0]);
			
			S2CShipChange* sc=(S2CShipChange*)pkt;
			Player *p=GetPlayer(sc->pid);
			
			char buf[1024];
			sprintf(buf,"%s changed to ship %i on freq %i",p->name,sc->ship,sc->freq);
			chat->RecieveChat(CHAT_GREY,buf);
			
			p->ship=sc->ship;
			p->freq=sc->freq;
			
			return TRUE;
		}
		break;
		case S2C_POSITION:
		{
			S2CPosition* pos=(S2CPosition*)pkt;
			Player *p=GetPlayer(pos->pid);
			
			p->Ship.x=pos->x;
			p->Ship.y=pos->y;
			p->Ship.xspeed=pos->xspeed;
			p->Ship.yspeed=pos->yspeed;
			p->Ship.rotation=pos->rotation;
			p->Ship.bounty=pos->bounty;
			p->Ship.status=pos->status;
			
			// printf("pid=%d=%s pos (%d,%d)\n",pos->pid,p->name,p->Ship.x,p->Ship.y);
			
			return TRUE;
		}
		break;
		
	}

	return FALSE;
}

local Iplayer plint=
{
	INTERFACE_HEAD_INIT(I_PLAYER,"player"),
	GetPlayer, GetPlayerByName, Player2PID, Name2PID
};

EXPORT int MM_player(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		gfx=mm->GetInterface(I_GFX);
		if(!lm || !net || !gfx) return MM_FAIL;
		
		net->AddPacket(S2C_ENTERINGARENA,packethandler);
		net->AddPacket(S2C_PLAYERENTERING,packethandler);
		net->AddPacket(S2C_PLAYERLEAVING,packethandler);
		
		net->AddPacket(S2C_KILL,packethandler);
		net->AddPacket(S2C_FREQCHANGE,packethandler);
		net->AddPacket(S2C_SHIPCHANGE,packethandler);
		
		net->AddPacket(S2C_POSITION,packethandler);
		
		mm->RegCallback(CB_DRAWGRAPHICS,DrawGraphics);
		
		mm->RegInterface(&plint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&plint)) return MM_FAIL;
		
		mm->UnregCallback(CB_DRAWGRAPHICS,DrawGraphics);
		
		net->RemovePacket(S2C_ENTERINGARENA,packethandler);
		net->RemovePacket(S2C_PLAYERENTERING,packethandler);
		net->RemovePacket(S2C_PLAYERLEAVING,packethandler);
		
		net->RemovePacket(S2C_KILL,packethandler);
		net->RemovePacket(S2C_FREQCHANGE,packethandler);
		net->RemovePacket(S2C_SHIPCHANGE,packethandler);
		
		net->RemovePacket(S2C_POSITION,packethandler);
		
		mm->ReleaseInterface(lm);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(gfx);
		mm->ReleaseInterface(chat);
		
		return MM_OK;
	}
	return MM_FAIL;
}

