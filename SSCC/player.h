
#ifndef __PLAYER_H
#define __PLAYER_H


typedef enum
{
	SHIP_WB=0,
	SHIP_JAV,
	SHIP_SPID,
	SHIP_LEV,
	SHIP_TERR,
	SHIP_WZL,
	SHIP_LANC,
	SHIP_SHK,
	SHIP_SPEC,
	
	SHIP_WARBIRD=0,
	SHIP_JAVELIN,
	SHIP_SPIDER,
	SHIP_LEVIATHAN,
	SHIP_TERRIER,
	SHIP_WEASEL,
	SHIP_LANCASTER,
	SHIP_SHARK,
	SHIP_SPECTATOR,
	
} ShipType;

typedef struct Ship
{
	i16 x;
	i16 y;
	i16 xspeed;
	i16 yspeed;
	i8 rotation;
	
	u16 bounty;
	i16 energy;
	u8 status;
	int flags;
	
	u32 shields		:1;
	u32 super		:1;
	u32 bursts		:4;
	u32 repels		:4;
	u32 thors		:4;
	u32 bricks		:4;
	u32 decoys		:4;
	u32 rockets		:4;
	u32 portals		:4;
} Ship;

typedef struct Score
{
	int kills;
	int deaths;
	int fpoints;
	int kpoints;
} Score;

typedef struct Player
{
	int pid;
	char name[30];
	char squad[30];
	int ship;
	int freq;
	BOOL aam;
	int turretee;
	BOOL koth;
	Ship Ship;
	Score Score;
	
} Player;

Player self;

#define I_PLAYER "player-1"

typedef struct Iplayer
{
	INTERFACE_HEAD_DECL;

	Player* (*GetPlayer)(int pid);
	Player* (*GetPlayerByName)(char* name);
	int (*Player2PID)(Player *p);
	int (*Name2PID)(char* name);
	
} Iplayer;


#endif

