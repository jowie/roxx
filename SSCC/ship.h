

#ifndef __SHIP_H
#define __SHIP_H


typedef enum
{
	LR_ZONEENTER=0,
	LR_ZONEEXIT,
	LR_ARENAENTER,
	LR_ARENAEXIT,
	
} LocationReason;

#define CB_LOCATIONCHANGE "locationchange"
typedef void (*LocationChangeFunc)(LocationReason reason);

#define CB_SHIPCHANGE "shipchange"
typedef void (*ShipChangeFunc)(int ship);

#define CB_FREQCHANGE "freqchange"
typedef void (*FreqChangeFunc)(int freq);

#define I_SHIP "keyboard-1"

typedef struct Iship
{
	INTERFACE_HEAD_DECL;

	void (*SetShip)(ShipType ship);
	void (*SetFreq)(int freq);
	
} Iship;


#endif

