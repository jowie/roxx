
#include <stdio.h>

#include "defs.h"
#include "module.h"
#include "logman.h"

local Imodman *mm;
local Ilogman *lm;

EXPORT int MM_data_sql(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		if(!lm) return MM_FAIL;
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}

