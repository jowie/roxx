
#include "sscd.h"

local Imodman *mm;
local Ilogman *lm;

local void SaveData(S2DRegisterServer *rs)
{
	lm->Log(L_INFO,"<dataman> saving: %s\n",rs->name);
	DO_CBS(CB_DATAFUNC,DataFunc,(rs));
}

local Idataman dataint=
{
	INTERFACE_HEAD_INIT(I_DATAMAN,"dataman"),
	SaveData
};

EXPORT int MM_dataman(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		if(!lm) return MM_FAIL;
		
		mm->RegInterface(&dataint);
		
		// mm->RegCallback(CB_DATAFUNC,SaveData);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&dataint)) return MM_FAIL;
		
		// mm->UnregCallback(CB_DATAFUNC,SaveData);
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}

