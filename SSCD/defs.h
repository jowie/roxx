
#ifndef __DEFS_H
#define __DEFS_H

#include <stdbool.h>
#include <stddef.h>

#undef ATTR_FORMAT
#undef ATTR_MALLOC
#undef ATTR_UNUSED
#if defined(__GNUC__) && __GNUC__ >= 3
#define ATTR_FORMAT(type, idx, first) __attribute__ ((format (type, idx, first)))
#define ATTR_MALLOC() __attribute__ ((malloc))
#define ATTR_UNUSED() __attribute__ ((unused))
#else
#define ATTR_FORMAT(type, idx, first)
#define ATTR_MALLOC()
#define ATTR_UNUSED()
#endif



#ifdef WIN32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <io.h>
#include <winsock.h>
#include <limits.h>
#include <malloc.h>
#include <windef.h>
#include <wincon.h>
#include <direct.h>

#define EXPORT __declspec(dllexport)

#ifndef PATH_MAX
#define PATH_MAX 4096
#endif
#ifndef NAME_MAX
#define NAME_MAX 256
#endif

#define usleep(x) Sleep((x)/1000)
#define sleep(x) Sleep((x)*1000)

#else // not WIN32

#define EXPORT
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
typedef int SOCKET;
#define INVALID_SOCKET (-1)


#endif



//OS detection
#define PLATFORM_WINDOWS	1
#define PLATFORM_MAC		2
#define PLATFORM_UNIX		3

#if defined(_WIN32)
#define PLATFORM PLATFORM_WINDOWS
#elif defined(__APPLE__)
#define PLATFORM PLATFORM_MAC
#else
#define PLATFORM PLATFORM_UNIX
#endif

//usage:
//#if PLATFORM == PLATFORM_WINDOWS




/** an alias to use to keep stuff local to modules */
#define local static

//useful stuff for lazy typing
#define null NULL
#define BOOL bool
#define TRUE true
#define FALSE false

//force safe version usage - i want to make this work
// #define free(x) afree(x)
// #define malloc(x) amalloc(x)
// #define atoi(x) aatoi(x)
// #define strcpy(x,y) strncpy(x,y,sizeof(y))


typedef unsigned char Byte;
typedef unsigned int Ticks;


//#ifdef WIN32
//#define PLATFORM_IS_WINDOWS
//#else
//#ifdef whateverlinuxthingis
//#define PLATFORM_IS_LINUX
//#else
//#ifdef whatevermacthingis
//#define PLATFORM_IS_MAC
//#endif


#include "sizes.h"
#include "util.h"





#endif

