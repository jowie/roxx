
#include "sscd.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Icore *core;
local Iconfig *cfg;

local int packethandler(NetLoc loc, Byte *pkt, int len, WebInfo* wi)
{
	switch(pkt[0])
	{
		case C2D_LISTREQUEST:
		{
			C2DListRequest *lr=(C2DListRequest*)pkt;
			lm->Log(L_INFO,"<dir_client> got client list request on %d: minplayers=%i",loc,lr->minplayers);
			
			return TRUE;
		}
		break;
	}
	
	return FALSE;
}

EXPORT int MM_dir_client(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		core=mm->GetInterface(I_CORE);
		cfg=mm->GetInterface(I_CONFIG);
		if(!lm || !net || !core || !cfg) return MM_FAIL;
		
		int port=cfg->GetInt(GLOBAL,"Listen","ClientPort",4990);
		net->CreateNewSocket(LOC_CLIENT,TRUE);
		net->CreateNewEndpoint(LOC_CLIENT,cfg->GetStr(GLOBAL,"Listen","ClientIP"),port);
		net->ConnectToLocation(LOC_CLIENT);
		core->UseCoreProtocol(LOC_CLIENT);
		
		lm->Log(L_INFO,"<dir_client> Listening on port %d",port);
		
		net->AddPacket(LOC_CLIENT,C2D_LISTREQUEST,packethandler);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		core->StopUsingCoreProtocol(LOC_CLIENT);
		
		net->RemovePacket(LOC_CLIENT,C2D_LISTREQUEST,packethandler);
		
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(core);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
