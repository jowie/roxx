
#include "sscd.h"

local Imodman *mm;
local Ilogman *lm;
local Inet *net;
local Icore *core;
local Iconfig *cfg;
local Idataman *dm;

/*
typedef struct S2DRegisterServer
{
	u32 ip;
	u16 port;
	u16 players;
	u16 scores;
	u32 version;
	char name[32];
	char password[32];
	char reserved[32];
	u8 desc[];
} S2DRegisterServer;
*/

local void UnhandledPacket(NetLoc loc, void *pkt, int len, WebInfo* wi)
{
	if(loc != LOC_SERVER) return;
	
	if(len < sizeof(S2DRegisterServer))
	{
		lm->Log(L_MALICIOUS,"<dir_server> packet recieved on %d but is too small %d < %d",loc,len,sizeof(S2DRegisterServer));
		return;
	}
	
	S2DRegisterServer *rs=(S2DRegisterServer*)pkt;
	lm->Log(L_INFO,"<dir_server> got server update on %d from %s",loc,rs->name);
}

EXPORT int MM_dir_server(Imodman *mm2, int action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		core=mm->GetInterface(I_CORE);
		cfg=mm->GetInterface(I_CONFIG);
		dm=mm->GetInterface(I_DATAMAN);
		if(!lm || !net || !core || !cfg || !dm) return MM_FAIL;
		
		int port=cfg->GetInt(GLOBAL,"Listen","ServerPort",4991);
		net->CreateNewSocket(LOC_SERVER,TRUE);
		net->CreateNewEndpoint(LOC_SERVER,cfg->GetStr(GLOBAL,"Listen","ServerIP"),port);
		net->ConnectToLocation(LOC_SERVER);
		core->UseCoreProtocol(LOC_SERVER);
		
		lm->Log(L_INFO,"<dir_server> Listening on port %d",port);
		
		mm->RegCallback(CB_PACKETARRIVED,UnhandledPacket);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		core->StopUsingCoreProtocol(LOC_SERVER);
		
		mm->UnregCallback(CB_PACKETARRIVED,UnhandledPacket);
		
		mm->ReleaseInterface(dm);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(core);
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
