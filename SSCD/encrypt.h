
#ifndef __ENCRYPT_H
#define __ENCRYPT_H

typedef struct Iencrypt
{
	INTERFACE_HEAD_DECL;

	void (*ConnectionInit)(NetLoc loc);
	int (*Encrypt)(Byte *pkt, int len);
	int (*Decrypt)(Byte *pkt, int len);
	
} Iencrypt;

#endif

