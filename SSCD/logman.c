
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "defs.h"
#include "module.h"
#include "logman.h"

local Imodman *mm;

local MPQueue queue;
local pthread_t thd;

void* LoggingThread(void *dummy)
{
	char *ll;

	while(TRUE)
	{
		ll=MPRemove(&queue);
		if(ll == NULL) return NULL;

		DO_CBS(CB_LOGFUNC,LogFunc,(ll));
		afree(ll);
	}
}

void Log(char level, const char *format, ...)
{
	int len, async=1;
	va_list argptr;
	char buf[1024];

	if(level & L_SYNC)
	{
		level &= 0x7f;
		async=0;
	}

	buf[0]=level;
	buf[1]=' ';

	va_start(argptr,format);
	len=vsnprintf(buf+2,1022,format,argptr);
	va_end(argptr);

	if(len > 0)
	{
		if(async) MPAdd(&queue,astrdup(buf));
		else DO_CBS(CB_LOGFUNC,LogFunc,(buf));
	}
}


local Ilogman logint=
{
	INTERFACE_HEAD_INIT(I_LOGMAN,"logman"),
	Log
};

EXPORT int MM_logman(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		MPInit(&queue);
		
		mm->RegInterface(&logint);
		
		pthread_create(&thd,NULL,LoggingThread,NULL);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&logint)) return MM_FAIL;
		
		MPAdd(&queue,NULL);
		pthread_join(thd,NULL);
		MPDestroy(&queue);
		
		return MM_OK;
	}
	
	return MM_FAIL;
}

