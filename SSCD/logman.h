
#ifndef __LOGMAN_H
#define __LOGMAN_H


/* priority levels */
#define L_DRIVEL     'D'  /**< really useless info */
#define L_INFO       'I'  /**< informative info */
#define L_MALICIOUS  'M'  /**< bad stuff from the client side */
#define L_WARN       'W'  /**< something bad, but can be recovered from */
#define L_ERROR      'E'  /**< something really really bad */
#define L_SYNC       0x80


#define CB_LOGFUNC "log"
typedef void (*LogFunc)(const char *line);


#define I_LOGMAN "logman-1"

typedef struct Ilogman
{
	INTERFACE_HEAD_DECL;

	void (*Log)(char level, const char *format, ...)
		ATTR_FORMAT(printf, 2, 3);

} Ilogman;


#endif
