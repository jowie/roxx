
#define MAKE_NET_PRINT_PACKETS
// #define MAKE_NET_PRINT_EXTRA_INFO

//#include <sys/types.h>

#include "sscd.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef WIN32
#include <errno.h>
#endif

#ifdef WIN32
#define close(x) closesocket(x)
#endif

local Imodman *mm;
local Ilogman *lm;
local Iencrypt *enc;

local pthread_t sthd;
local pthread_t rthd;

local LinkedList socketlist=LL_INITIALIZER;
local LinkedList packethandlers=LL_INITIALIZER;
// local LinkedList corehandlers=LL_INITIALIZER;

local fd_set cleanfds;
local int cleanfds_maxfds = 0;

local SocketID sid=0;
local NetLoc extralocs=100;

local pthread_mutex_t sidmtx=PTHREAD_MUTEX_INITIALIZER;
local pthread_mutex_t locmtx=PTHREAD_MUTEX_INITIALIZER;
local pthread_mutex_t socketmtx=PTHREAD_MUTEX_INITIALIZER;
local pthread_mutex_t packetmtx=PTHREAD_MUTEX_INITIALIZER;

typedef struct PacketHandler
{
	NetLoc loc;
	Byte type;
	PacketFunc func;
} PacketHandler;

typedef struct SocketInfo
{
	NetLoc loc;
	SocketID sid;
	SOCKET socket;
	WebInfo *wi;
	BOOL listenonly;
	// BOOL connected;
} SocketInfo;

local char* SocketErrorDesc(const int Error)
{
#ifdef WIN32
	switch(Error)
	{
		case WSAEINTR:				return "Interrupted system call";
		case WSAEBADF:				return "Bad file number";
		case WSAEACCES:				return "Permission denied";
		case WSAEFAULT:				return "Bad address";
		case WSAEINVAL:				return "Invalid argument";
		case WSAEMFILE:				return "Too many open files";
		case WSAEWOULDBLOCK:		return "Operation would block";
		case WSAEINPROGRESS:		return "Operation now in progress";
		case WSAEALREADY:			return "Operation already in progress";
		case WSAENOTSOCK:			return "Socket operation on non-socket";
		case WSAEDESTADDRREQ:		return "Destination address required";
		case WSAEMSGSIZE:			return "Message too long";
		case WSAEPROTOTYPE:			return "Protocol wrong type for socket";
		case WSAENOPROTOOPT:		return "Protocol not available";
		case WSAEPROTONOSUPPORT:	return "Protocol not supported";
		case WSAESOCKTNOSUPPORT:	return "Socket type not supported";
		case WSAEOPNOTSUPP:			return "Operation not supported on socket";
		case WSAEPFNOSUPPORT:		return "Protocol family not supported";
		case WSAEAFNOSUPPORT:		return "Address family not supported by protocol family";
		case WSAEADDRINUSE:			return "Address already in use";
		case WSAEADDRNOTAVAIL:		return "Can't assign requested address";
		case WSAENETDOWN:			return "Network is down";
		case WSAENETUNREACH:		return "Network is unreachable";
		case WSAENETRESET:			return "Network dropped connection on reset";
		case WSAECONNABORTED:		return "Software caused connection abort";
		case WSAECONNRESET:			return "Connection reset by peer";
		case WSAENOBUFS:			return "No buffer space available";
		case WSAEISCONN:			return "Socket is already connected";
		case WSAENOTCONN:			return "Socket is not connected";
		case WSAESHUTDOWN:			return "Can't send after socket shutdown";
		case WSAETOOMANYREFS:		return "Too many references: can't splice";
		case WSAETIMEDOUT:			return "Connection timed out";
		case WSAECONNREFUSED:		return "Connection refused";
		case WSAELOOP:				return "Too many levels of symbolic links";
		case WSAENAMETOOLONG:		return "File name too long";
		case WSAEHOSTDOWN:			return "Host is down";
		case WSAEHOSTUNREACH:		return "No route to host";
		case WSAENOTEMPTY:			return "Directory not empty";
		case WSAEPROCLIM:			return "Too many processes";
		case WSAEUSERS:				return "Too many users";
		case WSAEDQUOT:				return "Disc quota exceeded";
		case WSAESTALE:				return "Stale NTFS file handle";
		case WSAEREMOTE:			return "Too many levels of remote in path";
		case WSASYSNOTREADY:		return "Network sub-system is unusable";
		case WSAVERNOTSUPPORTED:	return "WinSock DLL cannot support this application";
		case WSANOTINITIALISED:		return "WinSock not initialized";
		case WSAHOST_NOT_FOUND:		return "Host not found";
		case WSATRY_AGAIN:			return "Non-authoritative host not found";
		case WSANO_RECOVERY:		return "Non-recoverable error";
		case WSANO_DATA:			return "No Data";
		default:					return "Not a WinSock error";
	}
#endif
	return NULL;
}

#ifndef WIN32

int set_nonblock(int socket)
{
        int flags;
        flags = fcntl(socket, F_GETFL, 0);

        if (flags == -1)
        {
                return flags;
        }

        return fcntl(socket, F_SETFL, flags | O_NONBLOCK);
}
#endif

local void printerror(char* msg)
{
#ifdef WIN32
	int err=WSAGetLastError();
	lm->Log(L_WARN,"%s: (#%d) %s",msg,err,SocketErrorDesc(err));
#else
	lm->Log(L_WARN,"%s: %s",msg,strerror(errno));
#endif
}

local void printpacket(Byte *pkt, int len, int dest, int core, int send)
{

printf("%s%son %d: size=%d\n",(send ? "send " : "recv "),(core ? "core " : ""),dest,len);
	int i=0;
	int c=0;
	for(i=0; i<len; i++)
	{
		if(c >= 16)
		{
			printf("\n");
			c=0;
		}
		printf("%02x ",pkt[i]);
		c++;
	}
	printf("\n");
}

local void printwi(WebInfo* wi, int loc, int send)
{
	int fam=wi->sin_family;
	char* addr=inet_ntoa(wi->sin_addr);
	// int port=ntohl(wi->sin_port);
	int port=wi->sin_port;
	
	printf("wi%son %d: fam=%d addr=%s port=%d\n",(send ? " send " : " recv "),loc,fam,addr,port);
	
	// struct sockaddr_in antelope;
	// char *some_addr;
	// inet_aton("10.0.0.1",&antelope.sin_addr); // store IP in antelope
	// some_addr=inet_ntoa(antelope.sin_addr); // return the IP
	// and this call is the same as the inet_aton() call, above:
	// antelope.sin_addr.s_addr=inet_addr("10.0.0.1");
}

local SocketInfo* getsocketbyid(SocketID id)
{
	SocketInfo* si=NULL;
	FOR_EACH_LINK(socketlist,si,l)
	{
		if(si->sid==id) return si;
	}

	return NULL;
}

local SocketInfo* getsocketbyloc(NetLoc loc)
{
	SocketInfo* si=NULL;
	FOR_EACH_LINK(socketlist,si,l)
	{
		if(si->loc==loc) return si;
	}

	return NULL;
}

local NetLoc CreateNewLocation(void)
{
	pthread_mutex_lock(&locmtx);
	extralocs++;
	pthread_mutex_unlock(&locmtx);
	
	return extralocs;
}

local void CreateNewSocket(NetLoc loc, BOOL listenonly)
{
	pthread_mutex_lock(&sidmtx);
	sid++;
	pthread_mutex_unlock(&sidmtx);
	
	//create socket
	SocketInfo* si=amalloc(sizeof(SocketInfo));
	si->socket=INVALID_SOCKET;
	si->socket=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
#ifdef MAKE_NET_PRINT_EXTRA_INFO
printf("created socket #%d on loc %d\n",si->socket,loc);
#endif
	if(si->socket == INVALID_SOCKET) lm->Log(L_WARN,"CreateNewSocket socket() failed");
	
#ifdef WIN32
	u_long noblock=1;
	if(ioctlsocket(si->socket,FIONBIO,&noblock) != 0)
	{
		printerror("bad ioctlsocket: %s");
	}
#else
	if(set_nonblock(si->socket) < 0) lm->Log(L_WARN,"<net> can't make socket nonblocking");
#endif
	
	// strncpy(si->name,name,20);
	si->loc=loc;
	si->sid=sid;
	si->listenonly=listenonly;
	
	//TODO FIXME memory leak maybe
	si->wi=malloc(sizeof(WebInfo));

	if (si->socket > cleanfds_maxfds)
	{
	        cleanfds_maxfds = si->socket;
	}

	FD_SET(si->socket,&cleanfds);
	
	pthread_mutex_lock(&socketmtx);
	LLAddLast(&socketlist,si);
	pthread_mutex_unlock(&socketmtx);
	
	// return sid;
}

local void CreateNewEndpoint(NetLoc loc, char* ip, int port)
{
	SocketInfo* si=getsocketbyloc(loc);
	if(!si)
	{
		lm->Log(L_ERROR,"<net> cant create endpoint on nonexistent socket at loc %i",loc);
//		return FALSE;
		return;
	}

	memset(si->wi,0,sizeof(WebInfo));
	si->wi->sin_family=AF_INET;
	// wi->sin_addr.s_addr=inet_addr(ip != null ? ip : ""); //msdn lies
	si->wi->sin_addr.s_addr=(ip != null) ? inet_addr(ip) : INADDR_ANY; //msdn lies
	si->wi->sin_port=htons(port);
	
	// return wi;
}

local BOOL ConnectToLocation(NetLoc loc)
{
	SocketInfo* si=getsocketbyloc(loc);
	if(!si)
	{
		lm->Log(L_ERROR,"<net> cant connect to nonexistent socket at loc %i",loc);
		return FALSE;
	}

	// if(si->listenonly) wi.sin_addr.s_addr=INADDR_ANY; //local might not want to force INADDR_ANY
	// si->connected=FALSE;
	
	if(si->listenonly)
	{
#ifdef MAKE_NET_PRINT_EXTRA_INFO
printf("binding socket #%d on loc %d\n",si->socket,loc);
#endif
		if(bind(si->socket,(struct sockaddr*)si->wi,sizeof(WebInfo)) == -1)
		{
			lm->Log(L_WARN,"<net> can't bind socket #%i",si->socket);
			printerror("bind failed");
			// closesocket(si->socket); //causes conflicting types compile errors on devcpp
			return FALSE;
		}
		// else si->connected=TRUE;
	}
	else
	{
#ifdef MAKE_NET_PRINT_EXTRA_INFO
printf("connecting socket #%d on loc %d\n",si->socket,loc);
#endif
		if(connect(si->socket,(struct sockaddr*)si->wi,sizeof(WebInfo)) == -1)
		{
			lm->Log(L_WARN,"ConnectToSocket connect() failed on %i",si->loc);
			printerror("connect failed");
		}
		// else si->connected=TRUE;
	}
	
	// return si->connected;
	return TRUE;
}

local void SetLocationEndpoint(NetLoc loc, WebInfo* wi)
{
	SocketInfo* si=getsocketbyloc(loc);
	if(!si)
	{
		lm->Log(L_ERROR,"<net> cant set endpoint on nonexistent socket at loc %i",loc);
//		return FALSE;
		return;
	}
	
	// si->wi=wi;
	memcpy(si->wi,wi,sizeof(WebInfo));
}

local void AddPacket(NetLoc loc, Byte type, PacketFunc func)
{
	PacketHandler *ph=amalloc(sizeof(PacketHandler));
	ph->loc=loc;
	ph->type=type;
	ph->func=func;
	
	pthread_mutex_lock(&packetmtx);
	LLAddLast(&packethandlers,ph);
	pthread_mutex_unlock(&packetmtx);
}

local void RemovePacket(NetLoc loc, Byte type, PacketFunc func)
{
	PacketHandler *ph;
	FOR_EACH_LINK(packethandlers,ph,l)
	{
		if((ph->type == type) && (ph->func == func))
		{
			LLRemove(&packethandlers,ph);
			break;
		}
	}
}

local BOOL SendPacket(NetLoc loc, void *vp, int size)
{
	SocketInfo* si=getsocketbyloc(loc);
	if(!si)
	{
		lm->Log(L_ERROR,"<net> cant send to nonexistent socket at loc %i",loc);
		return FALSE;
	}
	
	Byte *pkt=vp;

#ifdef MAKE_NET_PRINT_EXTRA_INFO
printf("send to loc %i: sock=%d\n",loc,si->socket);
printwi(si->wi,loc,1);
#endif
#ifdef MAKE_NET_PRINT_PACKETS
printpacket(pkt,size,loc,0,1);
#endif

	if(enc) enc->Encrypt(pkt,size);
	
	// if(send(si->socket,(char*)pkt,size,0) == -1) printerror("send failed");
	if(sendto(si->socket,(char*)pkt,size,0,(struct sockaddr*)si->wi,sizeof(WebInfo)) == -1) printerror("send failed");
	
	// afree(pkt);
	return TRUE;
}

local void ReceivePacket(NetLoc loc, void *vp, int len, WebInfo* wi)
{
	if(len < 1) return;
	
	SocketInfo* si=getsocketbyloc(loc);
	if(!si)
	{
		lm->Log(L_ERROR,"<net> cant send to nonexistent socket at loc %i",loc);
//		return FALSE;
		return;
	}
	
	Byte *pkt=vp;
	{ DO_CBS(CB_PACKETARRIVED,PacketArrivedFunc,(si->loc,pkt,len,wi)); }
	
	if(enc) enc->Decrypt(pkt,len);
	
	Byte type=pkt[0];
	Byte subtype=0;

	BOOL handled=FALSE;
	PacketHandler *ph;
	FOR_EACH_LINK(packethandlers,ph,l)
	{
// if(l) printf("link=%p next=%p prev=%p\n",l,l->next,l->prev);
// if(ph) printf("ph=%p loc=%d type=%d\n",ph,ph->loc,ph->type);
		if(ph->loc != loc) continue;
		if(ph->type != type) continue;
		
		if(ph->func(loc,pkt,len,wi))
		{
			handled=TRUE;
			// break; //ultimately not a good idea
		}
	}
	
	if(!handled) printf("unhandled packet type on %d: %02x\n",loc,pkt[0]);
	// else printpacket(pkt,len,loc,0,0);
}

local void* ReceiveThread(void *vp)
{
	struct timeval timeout;
	fd_set dirtyfds;

	FD_ZERO(&cleanfds);
	
	while(TRUE) //wait for a packet to arrive at one of the sockets
	{
	        int n = -1;

		do
		{
			pthread_testcancel(); //check for shutdown
			dirtyfds=cleanfds;
			// FD_COPY(cleanfds,dirtyfds);
			timeout.tv_sec=1;
			timeout.tv_usec=0;

		}
		while( (n = select(cleanfds_maxfds+1,&dirtyfds,NULL,NULL,&timeout)) < 1); //molests the timeout and fds values


		//broke out of loop means a packet arrived at one of the sockets
		SocketInfo* si=NULL;
		FOR_EACH_LINK(socketlist,si,l)
		{
			if(FD_ISSET(si->socket,&dirtyfds)) //the packet arrived at this socket
			{
				// if(si->connected)
				{
					WebInfo wi;
					unsigned int size=sizeof(WebInfo);
					unsigned char rbuf[1024];
					memset(rbuf,0,sizeof(rbuf));
					int bytes=recvfrom(si->socket,rbuf,sizeof(rbuf),0,(struct sockaddr*)&wi,&size);
					
					//if set nonblocking, will return -1 on nothing there
					if(bytes != -1)
					{
						Byte *pkt=(Byte*)rbuf;
#ifdef MAKE_NET_PRINT_EXTRA_INFO
printf("recv on loc %i: sock=%d\n",si->loc,si->socket);
printwi(&wi,si->loc,0);
#endif
#ifdef MAKE_NET_PRINT_PACKETS
printpacket(pkt,bytes,si->loc,0,0);
#endif
						// memcpy(si->wi,&wi,size); //TODO FIXME what happens when 2 places send to 1 loc?
						ReceivePacket(si->loc,pkt,bytes,&wi);
					}
				}
//				else lm->Log(L_INFO,"<net> got data on unconnected socket at loc %i",si->loc);
			}
		}

		fullsleep(5);
	}
}

local void SendThread(void *vp)
{
//implement packet queue here
	pthread_testcancel();
	fullsleep(10);
}

//local void RegisterEncryption(Iencrypt *ie)
local void RegisterEncryption(void *ie)
{
	enc=ie; //last one loaded wins!
}

local void init(void)
{

#ifdef WIN32
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2,2),&wsaData) != NO_ERROR) lm->Log(L_WARN,"bad wsastartup: %d",WSAGetLastError());
#endif
	
	//create send and recieve threads
	pthread_create(&sthd,NULL,SendThread,NULL);
	pthread_create(&rthd,NULL,ReceiveThread,NULL);
}

local Inet netint=
{
	INTERFACE_HEAD_INIT(I_NET,"net"),
	CreateNewLocation, CreateNewSocket, CreateNewEndpoint, ConnectToLocation, SetLocationEndpoint,
	AddPacket, RemovePacket,
	SendPacket, ReceivePacket,
	RegisterEncryption
};

EXPORT int MM_net(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		// enc=mm->GetInterface("enc-vie");
		enc=NULL; //to be gotten later
		if(!lm) return MM_FAIL;
		
		init();
		
		mm->RegInterface(&netint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&netint)) return MM_FAIL;
		
		//kill threads
		pthread_cancel(sthd);
		pthread_cancel(rthd);
		pthread_join(sthd,NULL);
		pthread_join(rthd,NULL);
		
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
