
#define MAKE_CORE_PRINT_PACKETS

#include "sscd.h"
#include <stdio.h>
#include <string.h>

local Imodman *mm;
local Ilogman *lm;
local Inet *net;

local LinkedList connections=LL_INITIALIZER;

local LinkedList chunks=LL_INITIALIZER;
int chunksum=0;
int chunksnum=0;
// local LinkedList stream=LL_INITIALIZER;
BOOL processingstream=FALSE;
int streamsize=0;
int maxstreamsize=0;
Byte *streamdata=NULL;
Byte *placeholder=NULL;

u32 lastsync=0;

local void printpacket(Byte *pkt, int len)
{
if(pkt[1]==3) return;
	printf("-------------------------\n");
	int i=0;
	int c=0;
	for(i=0; i<len; i++)
	{
		if(c >= 16)
		{
			printf("\n");
			c=0;
		}
		printf("%02x ",pkt[i]);
		c++;
	}
	printf("\n");
	printf("-------------------------\n");
	printf("\n");
}

local void printwi(WebInfo* wi, int loc)
{
	int fam=wi->sin_family;
	char* addr=inet_ntoa(wi->sin_addr);
	// int port=ntohl(wi->sin_port);
	int port=wi->sin_port;
	
	printf("saving on %d: fam=%d addr=%s port=%d\n",loc,fam,addr,port);
}

local NetLoc getconnectionbyloc(NetLoc loc)
{
	// NetLoc ret=0;
	// FOR_EACH_LINK(connections,&ret,l)
	{
		// if(si->loc==loc) return si;
	}

	return loc;
}

local int packethandler(NetLoc dest, Byte *pkt, int len, WebInfo* wi);
local void UseCoreProtocol(NetLoc loc)
{
	net->AddPacket(loc,CORE_PACKET,packethandler);
}

local void StopUsingCoreProtocol(NetLoc loc)
{
	net->RemovePacket(loc,CORE_PACKET,packethandler);
}

local int packethandler(NetLoc dest, Byte *pkt, int len, WebInfo* wi)
{
	if(len < 2) return 0;

	switch(pkt[0])
	{
		case CORE_PACKET:
		{
			switch(pkt[1])
			{
				case CORE_LOGINREQUEST:
				{
					CoreLoginRequest *clrq=(CoreLoginRequest*)pkt;
					
					if(len < sizeof(CoreLoginRequest))
					{
						printf("CoreLoginRequest packet recieved on %d but is too small %d < %d",dest,len,sizeof(CoreLoginRequest));
						return FALSE;
					}
					
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("CoreLoginRequest packet recieved on %d: size=%d key=%d proto=%d\n",dest,len,clrq->key,clrq->protocol);
#endif
					
					CoreLoginResponse clrs;
					int size=sizeof(CoreLoginResponse);
					memset(&clrs,0,size);
					clrs.type=CORE_PACKET;
					clrs.subtype=CORE_LOGINRESPONSE;
					// clrs.key= ~(clrq->key);
					clrs.key=clrq->key;
					
					NetLoc newloc=net->CreateNewLocation();
					net->CreateNewSocket(newloc,FALSE);
// printwi(wi,dest);
					net->SetLocationEndpoint(newloc,wi);
					net->ConnectToLocation(newloc);
					UseCoreProtocol(newloc);
					LLAddLast(&connections,(void*)newloc);
					
					net->SendPacket(newloc,&clrs,size);
					
					return TRUE;
				}
				break;
				case CORE_LOGINRESPONSE:
				{
					CoreLoginResponse *clr=(CoreLoginResponse*)pkt;
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("login response key=%i ~key=%i\n",clr->key,~clr->key);
#endif
					
					DO_CBS(CB_CONNECTIONREADY,ConnectionReadyFunc,(dest));
					
					return TRUE;
				}
				break;
				case CORE_RELHEADER:
				{
					CoreRelHeader *crh=(CoreRelHeader*)pkt;
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("rel pkt id=%u: size=%d\n",crh->packetid,len);
#endif
					
					Byte *data=pkt+sizeof(CoreRelHeader);
					int datalen=len-sizeof(CoreRelHeader);
					
					CoreRelAck cra;
					int size=sizeof(CoreRelAck);
					memset(&cra,0,size);
					cra.type=CORE_PACKET;
					cra.subtype=CORE_RELACK;
					cra.packetid=crh->packetid;
					
					net->SendPacket(dest,&cra,size);
					net->ReceivePacket(dest,data,datalen,wi);
					
					return TRUE;
				}
				break;
				case CORE_DISCONNECT:
				{
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("disconnect packet recieved on %d: size=%d\n",dest,len);
#endif
					DO_CBS(CB_DISCONNECT,DisconnectFunc,(dest));
					
					return TRUE;
				}
				break;
				case CORE_CLUSTER:
				{
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("cluster packet recieved: size=%d\n",len);
#endif
					
					int sofar=sizeof(CoreClusterHeader);
					
					CoreClusterHeader *cch=(CoreClusterHeader*)pkt;
					Byte *data=(Byte*)pkt+sizeof(CoreClusterHeader);
					Byte size=cch->size;
					
					// printf("\ncluster: size=%d type=%02x %02x\n",size,data[0],data[1]);
// printpacket(data,size);
					net->ReceivePacket(dest,data,size,wi);
						
					data+=size;
					sofar+=size;
						
					while(sofar < len)
					{
						size=*data;
						// printf("\ncluster: size=%d type=%02x %02x\n",size,data[1],data[2]);
// printpacket(data,size);
						net->ReceivePacket(dest,data+1,size,wi);
						data+=size+1;
						sofar+=size+1;
					}
					
					return TRUE;
				}
				break;
				case CORE_CHUNK:
				case CORE_LASTCHUNK:
				{
				
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("last cluster packet recieved: size=%d\n",len);
#endif
				
typedef struct CoreChunk
{
	u8 type; //0x00
	u8 subtype; //0x08
	u8 data[];
} CoreChunk;
					Byte* pk=amalloc(len);
					memcpy(pk,pkt,len);
					LLAddLast(&chunks,pk);
			
					chunksum+=len;
					chunksnum++;
				
					if(pkt[1] == CORE_LASTCHUNK)
					{
						
						Byte* npk=amalloc(chunksum-chunksnum*2);
						int copied=0;
						
						Byte* pk2=NULL;
						FOR_EACH_LINK(chunks,pk2,l)
						{
						
							CoreChunk *cc=(CoreChunk*)pk2;
							copied+=
						
							LLRemove(&chunks,pk2);
							afree(pk2);
						}
						
						net->ReceivePacket(dest,streamdata,streamsize,wi);
					}
				}
				break;
				case CORE_STREAM:
				{
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("stream packet recieved: size=%d\n",len);
#endif
					
					// if(!processingstream)
					// {
						// processingstream=TRUE;
						// LLEmpty(&stream);
					// LLAdd(&stream,buf);
					// }
					
					CoreStreamHeader *csh=(CoreStreamHeader*)pkt;
					int size=len-sizeof(CoreStreamHeader);
					if(!streamdata)
					{
						maxstreamsize=csh->totalsize;
						streamsize=0;
						streamdata=amalloc(maxstreamsize);
						placeholder=streamdata;
					}
					if(streamsize + size <= maxstreamsize)
					{
						memcpy(placeholder,csh->data,size);
						placeholder+=size;
						streamsize+=size;
// printf("new stream size=%d\n",streamsize);
					}
					else printf("too much stream data sent\n");
					
					if(streamsize >= csh->totalsize)
					{
// printf("streamsize=%d  > totalsize=%d\n",streamsize,csh->totalsize);
					
						net->ReceivePacket(dest,streamdata,streamsize,wi);
					
						placeholder=NULL;
						// afree(streamdata); //FIXME: memory leak, calling this here wouldnt work
						streamdata=NULL;
						streamsize=0;
						maxstreamsize=0;
						
						// processingstream=FALSE;
						// LLEmpty(&stream);
					}
					
	
					return TRUE;
				}
				break;
				case CORE_STREAMCANCEL:
				{
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("stream cancel packet recieved: size=%d\n",len);
#endif
					
					if(placeholder)
					{
						placeholder=NULL;
						afree(streamdata);
						streamdata=NULL;
						
						// processingstream=FALSE;
						// LLEmpty(&stream);
					}
					else printf("stream cancel packet recieved for nonexistant stream\n");
					
					CoreStreamCancelAck csca;
					int size=sizeof(CoreStreamCancelAck);
					csca.type=CORE_PACKET;
					csca.subtype=CORE_STREAMCANCELACK;
					net->SendPacket(dest,&csca,size);
					
					return TRUE;
				}
				break;
				case CORE_SYNCREQUEST:
				{
#ifdef MAKE_CORE_PRINT_PACKETS
					printf("sync request packet recieved: size=%d\n",len);
#endif
					
					CoreSyncRequest *cs=(CoreSyncRequest*)pkt;
					printf("sync: time=%d sent=%d recieved=%d\n",
					cs->localtime,
					cs->packetssent,
					cs->packetsreceived);
					
					u32 curtime=current_ticks()/10;

					CoreSyncResponse csr;
					int size=sizeof(CoreSyncResponse);
					memset(&csr,0,size);
					csr.type=CORE_PACKET;
					csr.subtype=CORE_SYNCRESPONSE;
					csr.elapsedtime=curtime-lastsync;
					csr.servertime=curtime;
					net->SendPacket(dest,&csr,size);
					
					lastsync=curtime;
					
					return TRUE;
				}
				break;
				default:
				printf("unhandled core packet type on %d: %02x\n",dest,pkt[1]);
			}
		}
		break;
		default:
		printf("core module recieving strange packet type on %d: %02x\n",dest,pkt[0]);
	}

	return FALSE;
}

local Icore coreint=
{
	INTERFACE_HEAD_INIT(I_CORE,"core"),
	UseCoreProtocol, StopUsingCoreProtocol
};

EXPORT int MM_proto_core(Imodman *mm2, mod_action action)
{
	if(action == MM_LOAD)
	{
		mm=mm2;
		lm=mm->GetInterface(I_LOGMAN);
		net=mm->GetInterface(I_NET);
		if(!lm || !net) return MM_FAIL;
		
		mm->RegInterface(&coreint);
		
		return MM_OK;
	}
	else if(action == MM_UNLOAD)
	{
		if(mm->UnregInterface(&coreint)) return MM_FAIL;
		
		mm->ReleaseInterface(net);
		mm->ReleaseInterface(lm);
		
		return MM_OK;
	}
	return MM_FAIL;
}
