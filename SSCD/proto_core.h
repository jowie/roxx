
#ifndef __CORE_H
#define __CORE_H

//exchange keys with a zone when called
#define CB_NEWCONNECTION "NewConnection"
typedef void (*NewConnectionFunc)(NetLoc loc);

//called when data can be sent
#define CB_CONNECTIONREADY "connectionready"
typedef void (*ConnectionReadyFunc)(NetLoc loc);

//called when other side wants to disconnect
#define CB_DISCONNECT "disconnect"
typedef void (*DisconnectFunc)(NetLoc loc);

#define I_CORE "core-1"

typedef struct Icore
{
	INTERFACE_HEAD_DECL;

	void (*UseCoreProtocol)(NetLoc loc);
	void (*StopUsingCoreProtocol)(NetLoc loc);
	
} Icore;


#endif

