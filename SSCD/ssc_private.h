/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef SSC_PRIVATE_H
#define SSC_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.0.1.1"
#define VER_MAJOR	0
#define VER_MINOR	0
#define VER_RELEASE	1
#define VER_BUILD	1
#define COMPANY_NAME	""
#define FILE_VERSION	""
#define FILE_DESCRIPTION	"SubSpace Continuum Directory"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	""
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	""
#define PRODUCT_NAME	""
#define PRODUCT_VERSION	""

#endif /*SSC_PRIVATE_H*/
