
#ifndef __SSC_H
#define __SSC_H

//include order is sensitive due to dependencies

#include "defs.h"

#include "module.h"
#include "cmod.h"

#include "packets.h"
#include "net.h"
#include "encrypt.h"
#include "proto_core.h"

#include "mainloop.h"
#include "logman.h"
#include "config.h"

#include "dataman.h"

#endif
